<!DOCTYPE html>
<html lang="tr">
<head>
    <meta charset="UTF-8">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>ÖZ FİNANS-İŞ SENDİKASI - RESMİ WEBSİTESİ</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('css/app.css?v=1.'.rand(0,999999)) }}">
    @yield('css')
</head>
<body>
    <!-- TOP BAR -->
    <div id="topBar">
        <div class="container">
            <div class="level">
                <div class="level-left"></div>
                <div class="level-right">
                    Dil Seçiniz:
                    <span class="dil">
                        <a href="{{ url('tr') }}">
                            <img src="{{ asset('img/tr.png?2') }}" alt="">
                            <span>Türkçe</span>
                        </a>
                    </span>
                    |
                    <span class="dil">
                        <a href="{{ url('en') }}">
                            <img src="{{ asset('img/us.png?2') }}" alt="">
                            <span>English</span>
                        </a>
                    </span>

                    <ul class="social-icons">
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="#"><i class="fa fa-rss"></i></a></li>
                        <div class="clear"></div>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- TOP BAR END -->
    <!-- HEADER -->
    <header id="header">
        <div class="container">
            <div class="columns">
                <!-- LOGO -->
                <div class="logo">
                    <a href="{{ url('/') }}">
                        <img src="{{ asset('img/logo.png') }}" alt="">
                    </a>
                </div>
                <!-- LOGO END -->
                <!-- MENU -->
                <div class="menu columns">
                    <div class="a">
                        <div class="big">KURUMSAL</div>
                        <div class="small">PROFİL</div>


                        <div class="subMenu">
                            <ul>
                                <li><a href="{{ url('yonetim-kurulu') }}">Yönetim Kurulu</a></li>
                                <li><a href="{{ url('baskandan-mesajlar') }}">Başkandan Mesajlar</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="a">
                        <a href="{{ url('subeler') }}">
                            <div class="big">ŞUBELER</div>
                            <div class="small">TEMSİLCİLİKLER</div>
                        </a>
                    </div>
                    <div class="a">
                        <a href="{{ url('anlasmalar') }}">
                            <div class="big">ÖZEL FIRSATLAR</div>
                            <div class="small">ANLAŞMALI KURUMLAR</div>
                        </a>
                    </div>
                </div>
                <!-- MENU END -->
                <!-- SEARCH -->
                <div class="search">
                    <form action="{{ url(app()->getLocale() . '/haberler') }}">
                        <input type="text" name="q" class="input">
                        <button class="button"><i class="fa fa-search"></i></button>
                    </form>
                </div>
                <!-- SEARCH END -->

                <div class="clear"></div>
            </div>
        </div>
    </header>
    <!-- HEADER END -->
    <!-- MENÜ -->
    <div class="container">
        <nav id="menu">
            <div class="level">
                <div class="level-left">
                    <ul>
                        <li><a href="mailto:bilgi@ozfinansis.org.tr">KURUMSAL E-POSTA</a></li>
                        <li><a href="{{ url('forum') }}">FORUM</a></li>
                        <li><a href="#">SİTE HARİTASI</a></li>
                        <div class="clear"></div>
                    </ul>
                </div>
                <div class="level-right">
                    <ul>
                        <li><a href="{{ url('/') }}">ANA SAYFA</a></li>
                        <li><a href="{{ url('/haberler') }}">HABERLER</a></li>
                        <li><a href="{{ url('/duyurular') }}">DUYURULAR</a></li>
                        <li><a href="{{ url(app()->getLocale() . '/sayfa/2-mevzuat') }}">MEVZUAT</a></li>
                        <li><a href="{{ url(app()->getLocale() . '/sayfa/3-hukuk') }}">HUKUK</a></li>
                        <li><a href="{{ url('/yayinlar') }}">YAYINLAR</a></li>
                        <li><a href="{{ url('/fotograflar') }}">FOTOĞRAFLAR</a></li>
                        <li><a href="{{ url('/videolar') }}">VİDEOLAR</a></li>
                        <li><a href="{{ url('/iletisim') }}">İLETİŞİM</a></li>
                        <div class="clear"></div>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
    <!-- MENÜ END-->
    @yield('content')
    <!-- FOOTER -->
    <div id="footer">
        <div class="container">
            <div class="columns">
                <div class="column is-9">
                    <div class="columns">
                        <div class="column">
                            <h2>ÖZ FİNANS-İŞ SENDİKASI</h2>
                            <div class="adres">
                                Beştepe Mahallesi Merhale Sokak <br> No:8 Yenimahalle/Ankara
                                <br>
                                <br>

                                0312 341 88 77

                                <br>
                                <br>

                                bilgi@ozfinansis.org.tr
                            </div>

                            <div class="icons">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-youtube"></i></a>
                                <a href="#"><i class="fa fa-rss"></i></a>
                            </div>
                        </div>
                        <div class="column">
                            <div class="menu_title">Site Menüsü</div>
                            <div class="columns">
                                <div class="column">
                                    <ul>
                                        <li><a href="#">Anasayfa</a></li>
                                        <li><a href="#">Kurumsal</a></li>
                                        <li><a href="#">Haberler</a></li>
                                        <li><a href="#">Duyurular</a></li>
                                        <li><a href="#">Basın Açıklamaları</a></li>
                                        <li><a href="#">Mevzuat</a></li>
                                        <li><a href="#">Toplu İş Sözleşmeleri</a></li>
                                    </ul>
                                </div>
                                <div class="column">
                                    <ul>
                                        <li><a href="#">Hukuk</a></li>
                                        <li><a href="#">Yayınlar</a></li>
                                        <li><a href="#">Anlaşmalı Kurumlar</a></li>
                                        <li><a href="#">Şube ve Temsilcilikler</a></li>
                                        <li><a href="#">e-Devlet Sendika Üyeliği</a></li>
                                        <li><a href="#">İletişim</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="column">
                    <div class="menu_title">
                        Mobil Uygulamalarımız
                    </div>
                    <div class="uygulamalar">
                        <img src="{{ asset('img/play.png') }}" alt="">
                    </div>
                    <div class="menu_title" style="margin-top:10px">
                        e-Bülten Üyeliği
                    </div>
                    <p style="color:#708199; font-size: 12px">
                        Sendikamız ile ilgili güncel gelişmelerden
                        haberdar olmak için lütfen mailinizi kaydedin
                    </p>
                    <br>
                    <div class="eBulten">
                        <input type="text" placeholder="E-Posta Adresiniz" class="input">
                        <button class="button"><i class="fa fa-send"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- FOOTER END -->
    <!-- BOTTOM BAR -->
    <div id="bottombar">
        <div class="container">
            <div class="level">
                <div class="level-left">
                    Copyright 2019, Öz Finans-İş Sendikası, Tüm Hakları Saklıdır
                </div>
                <div class="level-right">
                    <a href="#">Gizlilik Politikası</a>
                    -
                    <a href="#">Kişisel Verilerin Korunması</a>
                    -
                    <a href="#">Site Kullanım Politikası</a>
                </div>
            </div>
        </div>
    </div>
    <!-- BOTTOM BAR END -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="{{ asset('js/app.js?v=1.'.rand(0,9999)) }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    @yield('js')
</body>
</html>
