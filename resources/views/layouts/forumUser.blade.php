<div class="container">
    @if(!auth()->check())
        <div class="forum-user">
            <a href="{{ url(app()->getLocale() . "/forum/login") }}" class="btn">Giriş Yap</a>
            <a href="{{ url(app()->getLocale() . "/forum/register") }}" class="btn">Kayıt Ol</a>
        </div>
    @else
        <div class="forum-logged-user">
            <div class="user">
                <i class="fa fa-user"></i>
                Merhaba, <strong>{{ auth()->user()->name }}</strong>.
            </div>
        </div>
    @endif
</div>
