<!-- PRESIDENT -->
<div class="president">
    <div class="top">
        <strong>Ahmet Eroğlu</strong>
        <br>
        Genel Başkan
    </div>
    <div class="bottom">
        <span class="icons">
            <i class="fa fa-facebook"></i>
            <i class="fa fa-twitter"></i>
        </span>
        <a style="color:#fff" href="{{ url('/baskandan-mesajlar') }}">Başkandan Mesaj</a>
    </div>

    <img src="{{ asset('img/baskan.png') }}" alt="">
</div>
<!-- PRESIDENT END -->
<!-- OTHER NEWS -->
<div class="other_news">
    <div class="top">
        <div class="columns">
            <div class="column" @click="show='diger'">
                <h2 :class="show=='diger' ? 'active':''">DİĞER HABERLER</h2>
            </div>
            <div class="column" @click="show='son'">
                <h2 :class="show=='son' ? 'active':''">SON EKLENENLER</h2>
            </div>
        </div>
    </div>
    <div class="body">
        <div class="haberler">
            <!-- diğer -->
            <ul v-if="show == 'diger'">
                @foreach(\App\Haber::inRandomOrder()->where('lang', app()->getLocale())->where('basin_aciklamasi', false)->limit(3)->get() as $haber)
                <li>
                    <div class="columns">
                        <div class="column is-5">
                            <a href="{{ $haber->url() }}"><img src="{{ Voyager::image($haber->thumbnail('127x63', 'image')) }}" alt=""></a>
                        </div>
                        <div class="column">
                            <a href="{{ $haber->url() }}">
                                <h3>{{ $haber->title }}</h3>
                                <div class="date">
                                    <i class="fa fa-clock-o"></i>
                                    10 Haziran 2018 - 08:15
                                </div>
                            </a>
                        </div>
                    </div>
                </li>
                @endforeach
            </ul>
            <!-- diğer end -->
            <!-- son eklenen -->
            <ul v-if="show == 'son'">
                @foreach(\App\Haber::latest()->where('lang', app()->getLocale())->where('basin_aciklamasi', false)->limit(3)->get() as $haber)
                <li>
                    <div class="columns">
                        <div class="column is-5">
                            <a href="{{ $haber->url() }}"><img src="{{ Voyager::image($haber->thumbnail('127x63', 'image')) }}" alt=""></a>
                        </div>
                        <div class="column">
                            <a href="{{ $haber->url() }}">
                                <h3>{{ $haber->title }}</h3>
                                <div class="date">
                                    <i class="fa fa-clock-o"></i>
                                    10 Haziran 2018 - 08:15
                                </div>
                            </a>
                        </div>
                    </div>
                </li>
                @endforeach
            </ul>
            <!-- son eklenen end -->
        </div>
    </div>
    <div class="bottom">
        <a href="{{ url('haberler') }}">TÜM HABERLER</a>
    </div>
</div>
<!-- OTHER NEWS END -->
<!-- OTHER NEWS -->
<div class="other_news2">
    <div class="top">
        <div class="columns">
            <div class="column">
                <h2 :class="show=='diger' ? 'active':''">İLGİLİ KATEGORİLER</h2>
            </div>
        </div>
    </div>
    <div class="body">
        <div class="menu">
            <ul>
                <li><a href="{{ url('basin-aciklamalari') }}">BASIN AÇIKLAMALARI</a></li>
                <li><a href="{{ url('duyurular') }}">DUYURULAR</a></li>
                <li><a href="{{ url('haberler') }}">HABERLER</a></li>
            </ul>
        </div>
    </div>
</div>
<br><br>
<!-- OTHER NEWS END -->
