@extends('layouts.app')

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css">
@stop

@section('content')
<!-- HOME_PRESIDENT-->
<div class="home-president">
    <div class="container">
        <div class="left">
            <!-- PRESIDENT -->
            <div class="president">
                <div class="top">
                    <strong>Ahmet Eroğlu</strong>
                    <br>
                    Genel Başkan
                </div>
                <div class="bottom">
                    <a href="{{ url('/baskandan-mesajlar') }}" style="color:#fff;">Başkandan Mesaj</a>
                </div>

                <img src="{{ asset('img/baskan.png') }}" alt="">
            </div>
            <!-- PRESIDENT END -->
            <!-- LINKLER -->
            <div class="linkler">
                <div class="link">
                    <a href="{{ url('fotograflar') }}">
                        <i class="fa fa-photo"></i>
                        <span>FOTO GALERİ</span>
                    </a>
                </div>
                <div class="link">
                    <a href="{{ url('videolar') }}">
                        <i class="fa fa-film"></i>
                        <span>VİDEO GALERİ</span>
                    </a>
                </div>
                <div class="link">
                    <a href="{{ url('yayinlar') }}">
                        <i class="fa fa-archive"></i>
                        <span>YAYINLAR</span>
                    </a>
                </div>
            </div>
            <!-- LINKLER END -->
        </div>

        <div class="right">
            <div class="slider">
                <div class="owl-carousel owl-theme">
                    @foreach($slider_haberler as $haber)
                    <div class="item">
                        <div class="haber">
                            <a href="{{ $haber->url() }}">
                                <img src="{{ Voyager::Image($haber->thumbnail('slider')) }}" alt="">
                                <div class="info">
                                    {{ $haber->title }}
                                </div>
                            </a>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="clear"></div>
    </div>
</div>
<!-- HOME_PRESIDENT END-->

<!-- HOME_LINKLER -->
<div class="container">
    <div class="home-linkler">
        <div class="columns">
            <!-- item -->
            <div class="column item">
                <a href="{{ url('basinda-biz') }}">
                    <div class="columns">
                        <div class="column is-3">
                            <i class="fa fa-map-o"></i>
                        </div>
                        <div class="column">
                            <span>Basında Biz</span> <br>
                            BASINDA ÖZ FİNANS-İŞ
                        </div>
                    </div>
                </a>
            </div>
            <!-- item end -->
            <!-- item -->
            <div class="column item">
                <a href="{{ url('duyurular') }}">
                    <div class="columns">
                        <div class="column is-3">
                            <i class="fa fa-bullhorn"></i>
                        </div>
                        <div class="column">
                            <span>Güncel Duyurular</span> <br>
                            DUYURULAR
                        </div>
                    </div>
                </a>
            </div>
            <!-- item end -->
            <!-- item -->
            <div class="column item">
                <a href="{{ url('haberler')  }}">
                    <div class="columns">
                        <div class="column is-3">
                            <i class="fa fa-newspaper-o"></i>
                        </div>
                        <div class="column">
                            <span>Güncel Haberler</span> <br>
                            HABERLER
                        </div>
                    </div>
                </a>
            </div>
            <!-- item end -->
            <!-- item -->
            <div class="column item">
                <a href="{{ url('forum') }}">
                    <div class="columns">
                        <div class="column is-3">
                            <i class="fa fa-comments-o"></i>
                        </div>
                        <div class="column">
                            <span>Sesinizi Duyurun</span> <br>
                            ÖZ FİNANS-İŞ FORUM
                        </div>
                    </div>
                </a>
            </div>
            <!-- item end -->
        </div>
    </div>
</div>
<!-- HOME_LINKLER END -->

<!-- HOME BOTTOM -->
<div class="container">
    <div class="home-bottom">
        <div class="columns">
            <!-- UYE OL -->
            <div class="column is-4">
                <div class="uyeol">
                    Sendikamıza üye olun
                    emeğinize sahip çıkın

                    <a href="#" class="button">e-Devlet'ten üye olmak için tıklayın</a>
                </div>
            </div>
            <!-- UYE OL END-->
            <!-- HOME HABERLER -->
            <div class="column">
                <div id="HomeHaberlerSlider" v-cloak class="home-haberler">
                    <div class="home_haberler_title">
                        <div class="level">
                            <div class="level-left">
                                <h2>ÖZ FİNANS-İŞ HABERLER</h2>
                            </div>
                            <div class="level-right">
                                <a href="#"> Tüm Haberler </a>
                            </div>
                        </div>
                    </div>
                    <div class="home_haberler_content">
                        <input type="hidden" id="home_haberler_count" value="2">
                        @php $i = 1; @endphp
                        @foreach($alt_haberler as $haber)
                            <div class="haber" v-if="currentHaber == {{$i++}}">
                                <div class="columns">
                                    <div class="column is-7">
                                        <a href="{{ $haber->url() }}">
                                            <img src="{{ Voyager::image($haber->thumbnail('500x300', 'image')) }}" alt="">
                                        </a>
                                    </div>
                                    <div class="column">
                                        <a href="{{ $haber->url() }}">
                                            <h3>{{ $haber->title }}</h3>
                                            <div class="date">{{ turkcetarih_formati('d F Y, l', $haber->created_at) }}</div>
                                            <p>
                                                {{ $haber->summary }}
                                            </p>
                                        </a>

                                        <div class="dots">
                                            <span v-for="i in haberSayisi" @click="currentHaber = i"
                                                :class="currentHaber == (i) ? 'aktif':''"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <!-- HOME HABERLER END -->
        </div>
    </div>
</div>
<!-- HOME BOTTOM END -->
<!-- ANLASMALAR -->
<div class="anlasmalar">
    <div class="container">
        <div class="top">
            <div class="columns">
                <div class="column is-4">
                    <h1>KURUMSAL<br><span>ANLAŞMALARIMIZ</span></h1>
                </div>
                <div class="column is-5">
                    <p>
                        Üyelerimiz için çok özel anlaşmalar. <br>
                        Öz Finans-İş Sendikası üyelerimiz çok özel ayrıcalıklardan faydalanıyor
                    </p>
                </div>
                <div class="column">
                    <div class="buttons">
                        <button class="button" @click="before" v-if="showBefore"><i class="fa fa-arrow-left"></i></button>
                        <button class="button" @click="next" v-if="showNext"><i class="fa fa-arrow-right"></i></button>
                    </div>
                </div>
            </div>
        </div>

        <div class="photos">
            <input type="hidden" id="anlasmalar_count" value="{{ count($anlasmalar) }}">
            <div class="columns">
                @php $i=0; @endphp
                @foreach($anlasmalar as $anlasma)
                <div class="column is-4" v-if="goster({{ $i++ }})">
                    <a href="{{ $anlasma->url() }}">
                        <img src="{{ Voyager::Image($anlasma->thumbnail('500x300')) }}" alt="">
                    </a>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
<!-- ANLASMALAR END -->
@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
    <script>
        $('.owl-carousel').owlCarousel({
            loop:true,
            margin:10,
            nav:false,
            dots: false,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1000:{
                    items:1
                }
            }
        });
    </script>
@endsection
