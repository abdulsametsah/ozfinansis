@extends('layouts.app')

@section('content')
<div id="pageTitle">
    <div class="container">
        <div class="level">
            <div class="level-left">
                <h1>Duyuru</h1>
            </div>
            <div class="level-right">
                <div class="links">
                    <a href="#">
                        <i class="fa fa-home"></i> Anasayfa
                    </a>
                    <i class="fa fa-angle-right"></i>
                    <a href="#">
                        Duyurular
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="haber">
    <div class="container">
        <div class="columns">
            <div class="column is-8">
                <img src="{{ Voyager::image($duyuru->image) }}" style="max-width:100%" alt="">
                <br> 
                <h1>{{ $duyuru->title }}</h1>
                <br><br>
                {!! $duyuru->body !!}
            </div>
            <div class="column">
                @include('layouts.sidebar')
            </div>
        </div>
    </div>
</div>
@stop
