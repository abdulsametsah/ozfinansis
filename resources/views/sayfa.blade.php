@extends('layouts.app')

@section('content')
<div id="pageTitle">
    <div class="container">
        <div class="level">
            <div class="level-left">
                <h1>{{ $sayfa->title }}</h1>
            </div>
            <div class="level-right">
                <div class="links">
                    <a href="#">
                        <i class="fa fa-home"></i> Anasayfa
                    </a>
                    <i class="fa fa-angle-right"></i>
                    <a href="#">
                        {{ $sayfa->title }}
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="haber">
    <div class="container">
        <div class="columns">
            <div class="column is-8">
                {!! $sayfa->body !!}
            </div>
            <div class="column">
                @include('layouts.sidebar')
            </div>
        </div>
    </div>
</div>
@stop
