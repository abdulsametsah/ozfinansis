@extends('layouts.app')

@section('content')
    <div id="pageTitle">
        <div class="container">
            <div class="level">
                <div class="level-left">
                    <h1>Forum</h1>
                </div>
                <div class="level-right">
                    <div class="links">
                        <a href="#">
                            <i class="fa fa-home"></i> Anasayfa
                        </a>
                        <i class="fa fa-angle-right"></i>
                        <a href="#">
                            Kurumsal
                        </a>
                        <i class="fa fa-angle-right"></i>
                        <a href="#">
                            Yönetim Kurulu
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include("layouts.forumUser")

    <div id="forum_index">
        <div class="container">
            @if (auth()->check())
            <br>
            <div class="level">
                <div class="level-left"></div>
                <div class="level-right">
                    <a href="#" class="button is-danger">YENİ KONU AÇ</a>
                </div>
            </div>
            <br>
            @endif

            <table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
                <tr>
                    <th>Görevde Yükselme İle İlgili Konular</th>
                    <th style="text-align:center">MESAJ</th>
                    <th>SON MESAJ ZAMANI</th>
                </tr>
                @foreach($konular as $konu)
                    <tr>
                        <td>
                            <a href="{{ url(app()->getLocale() . "/forum/" . $forum->id . "/" . $konu->id) }}">
                                <div class="baslik">{{ $konu->name }}</div>
                                <div class="desc">{{ $konu->description }}</div>
                            </a>
                        </td>
                        <td>
                            <div class="sayi">{{ number_format(count($konu->mesajlar())) }}</div>
                        </td>
                        <td>
                            @if ($konu->sonMesaj())
                                Son Mesaj:
                                <a href="#" style="color: blue">
                                    {{ $konu->sonMesaj()->user()->name }}
                                </a> <br>
                                {{ $konu->sonMesaj()->timeAgo() }}
                            @else
                                Henüz cevap yazılmadı.
                            @endif
                        </td>
                    </tr>
                @endforeach
            </table>
            {!! $konular->links() !!}

            <br><br>
        </div>
    </div>
@stop
