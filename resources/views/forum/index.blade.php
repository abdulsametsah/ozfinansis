@extends('layouts.app')

@section('content')
<div id="pageTitle">
    <div class="container">
        <div class="level">
            <div class="level-left">
                <h1>Forum</h1>
            </div>
            <div class="level-right">
                <div class="links">
                    <a href="#">
                        <i class="fa fa-home"></i> Anasayfa
                    </a>
                    <i class="fa fa-angle-right"></i>
                    <a href="#">
                        Kurumsal
                    </a>
                    <i class="fa fa-angle-right"></i>
                    <a href="#">
                        Yönetim Kurulu
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

@include("layouts.forumUser")

<div id="forum_index">
    <div class="container">
        <table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
            <tr>
                <th>FORUM</th>
                <th style="text-align:center">KONU</th>
                <th style="text-align:center">MESAJ</th>
                <th>SON MESAJ ZAMANI</th>
            </tr>
            @foreach($forumlar as $forum)
                <tr>
                    <td>
                        <a href="{{ url(app()->getLocale() . "/forum/" . $forum->id) }}">
                            <div class="baslik">{{ $forum->name }}</div>
                            <div class="desc">{{ $forum->description }}</div>
                        </a>
                    </td>
                    <td><div class="sayi">{{ count($forum->konular()) }}</div></td>
                    <td><div class="sayi">{{ number_format($forum->mesajSayisi()) }}</div></td>
                    <td>
                        @if ($forum->sonMesaj())
                            Son Mesaj:
                            <a href="#" style="color: blue">
                                {{ $forum->sonMesaj()->user()->name }}
                            </a> <br>
                            {{ $forum->sonMesaj()->timeAgo() }}
                        @else
                            Henüz cevap yazılmadı.
                        @endif
                    </td>
                </tr>
            @endforeach
        </table>

        <br><br>
    </div>
</div>
@stop
