@extends("layouts.app")

@section("content")
    <div id="pageTitle">
        <div class="container">
            <div class="level">
                <div class="level-left">
                    <h1>
                        Forum
                        <span>/ Giriş Yap</span>
                    </h1>
                </div>
                <div class="level-right">
                    <div class="links">
                        <a href="#">
                            <i class="fa fa-home"></i> Anasayfa
                        </a>
                        <i class="fa fa-angle-right"></i>
                        <a href="#">
                            Forum
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="forum-register">
            <form action="" id="step1">
                <label for="email">E-Posta Adresiniz</label>
                <input type="text" id="email" name="email">
                <label for="password">Şifreniz</label>
                <input type="password" id="password" name="password">
                <button id="check-button">Giriş Yap</button>
            </form>
        </div>
    </div>
@endsection

@section("js")
    <script>
        const checkButton = document.getElementById("check-button");
        checkButton.addEventListener("click", (e) => {
            e.preventDefault();

            const email = document.getElementById("email").value;
            const password = document.getElementById("password").value;

            $.ajax({
                url: '{{ url(app()->getLocale() . "/forum/login") }}',
                method: "post",
                data: {
                    _token: "{{ csrf_token() }}",
                    email, password
                },
                success: (response) => {
                    if (response.status === "error") {
                        return Swal.fire(response.message);
                    }else {
                        window.location.href = "{{ url(app()->getLocale() . '/forum') }}";
                    }
                }
            });
        });
    </script>
@endsection
