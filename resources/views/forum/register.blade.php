@extends("layouts.app")

@section("content")
    <div id="pageTitle">
        <div class="container">
            <div class="level">
                <div class="level-left">
                    <h1>
                        Forum
                        <span>/ Kayıt Ol</span>
                    </h1>
                </div>
                <div class="level-right">
                    <div class="links">
                        <a href="#">
                            <i class="fa fa-home"></i> Anasayfa
                        </a>
                        <i class="fa fa-angle-right"></i>
                        <a href="#">
                            Forum
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="forum-register">
            <div class="info">
                Foruma sadece sendika üyelerimiz kayıt olabilmektedir.
                Üyeliğinizi doğrulayabilmemiz için TC Kimlik Numaranızı ve telefon numaranızı yazınız.
            </div>

            <form action="" id="step1">
                <label for="tc">TC Kimlik Numaranız</label>
                <input type="text" id="tc" name="tc">
                <label for="telefon">Telefon Numaranız</label>
                <input type="text" id="telefon" name="telefon" placeholder="05XXXXXXXXXX">
                <button id="check-button">Kontrol Et</button>
            </form>
            <form action="" id="step2" style="display: none">
                <label for="name">Adınız Soyadınız</label>
                <input type="text" id="name" name="name">
                <label for="email">E-Posta Adresiniz</label>
                <input type="text" id="email" name="email">
                <label for="password">Şifreniz</label>
                <input type="password" id="password" name="password">
                <button id="register-button">Kayıt Ol</button>
            </form>
        </div>
    </div>
@endsection

@section("js")
    <script>
        const checkButton = document.getElementById("check-button");
        checkButton.addEventListener("click", (e) => {
            e.preventDefault();

            const tc = document.getElementById("tc").value;
            const telefon = document.getElementById("telefon").value;

            if (tc.length !== 11) {
                return Swal.fire("TC 11 karakterden oluşmalı.");
            }

            if (telefon.length !== 11) {
                return Swal.fire("Telefon 11 karakterden oluşmalı. Başında 0 olmalı.");
            }

            $.ajax({
                url: '{{ url(app()->getLocale() . "/forum/register") }}',
                method: "post",
                data: {
                    _token: "{{ csrf_token() }}",
                    tc, telefon
                },
                success: (response) => {
                    if (response.status === "error") {
                        return Swal.fire(response.message);
                    }else {
                        Swal.fire("Bilgileriniz doğru. Şimdi kayıt olabilirsiniz.")
                        .then(() => {
                            document.getElementById("step1").style.display = "none";
                            document.getElementById("step2").style.display = "";
                        });
                    }
                }
            });
        });

        const registerButton = document.getElementById("register-button");
        registerButton.addEventListener('click', (e) => {
            e.preventDefault();

            const name = document.getElementById("name").value;
            const email = document.getElementById("email").value;
            const password = document.getElementById("password").value;
            const tc = document.getElementById("tc").value;
            const telefon = document.getElementById("telefon").value;

            $.ajax({
                url: '{{ url(app()->getLocale() . "/forum/register-step2") }}',
                method: "post",
                data: {
                    _token: "{{ csrf_token() }}",
                    tc, telefon, name, email, password
                },
                success: (response) => {
                    if (response.status === "error") {
                        return Swal.fire(response.message);
                    }else {
                        Swal.fire(response.message).then(() => {
                            window.location.href = "{{ url(app()->getLocale() . "/forum") }}";
                        });
                    }
                }
            });
        });
    </script>
@endsection
