@extends('layouts.app')

@section('content')
<div id="duyurularVue">

    <div id="pageTitle">
        <div class="container">
            <div class="level">
                <div class="level-left">
                    <h1>Duyurular</h1>
                </div>
                <div class="level-right">
                    <div class="links">
                        <a href="#">
                            <i class="fa fa-home"></i> Anasayfa
                        </a>
                        <i class="fa fa-angle-right"></i>
                        <a href="#">
                            Duyurular
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div id="page_title2">
            <h1>Duyurular</h1>
            <div class="desc">
                Öz Finans-İş Sendikası Duyurular
            </div>
            <div class="turButtons">
                <a href="?type=flow" style="color:#000;"><i class="fa fa-list"></i></a>
                <a href="?type=grid" style="color:#000;"><i class="fa fa-th"></i></a>
            </div>
        </div>
    </div>

    <div id="duyurular">

        <div class="container">
            @if ($type == 'flow')
                <div class="cubuk"></div>
                <div class="duyurular">
                    <span>
                        @php $i = 1; @endphp
                        @foreach($duyurular as $duyuru)
                            @php $i++; @endphp
                            <article :class="'duyuru ' + ({{ $i }}%2 == 0 ? 'duyuru-sol':'')">
                                <a href="{{ $duyuru->url() }}">
                                    <div class="tarih">
                                        <span>{{ turkcetarih_formati('d F Y, l', $duyuru->created_at) }}</span>
                                    </div>
                                    <div class="body">
                                        <div class="columns">
                                            <div class="column is-8">
                                                <h2 style="color:#000">{{ $duyuru->title }}</h2>
                                                <p>
                                                    {{ $duyuru->summary }}
                                                </p>
                                            </div>
                                            <div class="column">
                                                <img src="{{ Voyager::image($duyuru->thumbnail('kare', 'image')) }}" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="nokta"><span></span></div>
                                    <div class="clear"></div>
                                </a>
                            </article>
                        @endforeach
                        <div class="clear"></div>
                        <br>
                        @if ($duyurular->lastPage() > 1)
                        <div class="more" style="display:none;">
                            <i @click="next" class="fa fa-angle-double-down"></i>
                        </div>
                        @endif
                    </span>
                </div>
            @else
                <div class="duyurular2">
                    <div class="columns is-multiline">
                        @foreach($duyurular as $duyuru)
                            <div class="column is-3">
                                <div class="duyuru">
                                    <a href="{{ $duyuru->url() }}">
                                        <img src="{{ Voyager::image($duyuru->thumbnail('kare', 'image')) }}" alt="">
                                        <div class="tarih">
                                            <span>{{ turkcetarih_formati('d', $duyuru->created_at) }}</span>
                                            <div>{{ turkcetarih_formati('M', $duyuru->created_at) }}</div>
                                        </div>
                                        <div class="info">
                                            <div class="title">{{ $duyuru->title }}</div>
                                            <p>{{ $duyuru->summary }}</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            @endif
        </div>

    </div>

    <div class="container">


        {!! $duyurular->links() !!}
    </div>
</div>

@stop
