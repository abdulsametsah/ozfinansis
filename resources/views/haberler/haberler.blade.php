@extends('layouts.app')

@section('content')
<div id="pageTitle">
    <div class="container">
        <div class="level">
            <div class="level-left">
                <h1>
                    Haberler
                    @if ($q)
                        <span>/ Ara: {{ $q }}</span>
                    @endif
                </h1>
            </div>
            <div class="level-right">
                <div class="links">
                    <a href="{{ url('/') }}">
                        <i class="fa fa-home"></i> Anasayfa
                    </a>
                    <i class="fa fa-angle-right"></i>
                    <a href="#">
                        Haberler
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="duyurular">
    <div class="container">
        <div class="duyurular2">
            <div class="columns is-multiline">
                
                @foreach($haberler as $haber)
                    <div class="column is-3">
                        <div class=" duyuru">
                            <a href="{{ $haber->url() }}">
                                <img src="{{ Voyager::image($haber->thumbnail('kare', 'image')) }}" alt="">
                                <div class="tarih">
                                    <span>{{ turkcetarih_formati('d', $haber->created_at) }}</span>
                                    <div>{{ turkcetarih_formati('M', $haber->created_at) }}</div>
                                </div>
                                <div class="info">
                                    <div class="title">{{ $haber->title }}</div>
                                    <p>{{ $haber->summary }}</p>
                                </div>
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

        <br><br>
        @if ($haberler->lastPage() > 1)
            {!! $haberler->links() !!}
        @endif
    </div>
</div>
@endsection
