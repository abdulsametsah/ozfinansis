@extends('layouts.app')

@section('css')
    <link href="https://vjs.zencdn.net/7.5.5/video-js.css" rel="stylesheet">
@endsection

@section('content')
<div id="pageTitle">
    <div class="container">
        <div class="level">
            <div class="level-left">
                <h1>Haber</h1>
            </div>
            <div class="level-right">
                <div class="links">
                    <a href="#">
                        <i class="fa fa-home"></i> Anasayfa
                    </a>
                    <i class="fa fa-angle-right"></i>
                    <a href="#">
                        Kurumsal
                    </a>
                    <i class="fa fa-angle-right"></i>
                    <a href="#">
                        Yönetim Kurulu
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="haber">
    <div class="container">
        <div class="columns">
            <div class="column is-8">
                <div class="cover">
                    <img src="{{ Voyager::image($haber->image) }}" alt="">
                    @if ($haber->video || $haber->photos)
                    <div class="icons2">
                        @if ($haber->video && isset(json_decode($haber->video)[0]))
                        <div class="icon2 openVideo">
                            <i class="fa fa-play-circle"></i>
                            <span>VİDEO</span>
                        </div>
                        @endif
                        @if ($haber->photos)
                        <div class="icon2 openPhotos" style="background: #a3620a;">
                            <i class="fa fa-camera"></i>
                            <span>GALERİ</span>
                            
                        </div>
                        @endif
                    </div>
                    @endif
                </div>
                <div class="modal videoModal">
                    <div class="modal-background"></div>
                    <div class="modal-content">
                        @if($haber->video && isset(json_decode($haber->video)[0]))
                        <video width="700" height="240" id="haberVideo" controls>
                            <source src='{{ asset('storage/' . json_decode($haber->video)[0]->download_link) }}' type='video/mp4'>
                        </video>
                        @endif
                    </div>
                    <button class="modal-close closeVideo is-large" aria-label="close"></button>
                </div>
                
                <div class="modal photosModal">
                    <div class="modal-background"></div>
                    <div class="modal-content">
                        @if($haber->photos)
                            @foreach(json_decode($haber->photos) as $photo)
                            <img src="{{ asset('storage/' . $photo) }}" height="100" alt="">
                            @endforeach
                        @endif
                    </div>
                    <button class="modal-close closePhotos is-large" aria-label="close"></button>
                </div>
                <h1>{{ $haber->title }}</h1>
                <div class="info">
                    <span>
                        <i class="fa fa-calendar-o"></i>
                        {{ turkcetarih_formati('d F Y, l', $haber->created_at) }}
                    </span>
                    <span>
                        <i class="fa fa-clock-o"></i>
                        {{ turkcetarih_formati('H:m', $haber->created_at) }}
                    </span>
                    <span>
                        <i class="fa fa-comment"></i>
                        {{ number_format($haber->comments) }}
                    </span>
                </div>
                @if ($haber->summary)
                <div class="summary">
                    <div class="columns">
                        <div class="column is-1" style="text-align:right;">
                            <i class="fa fa-quote-left"></i>
                        </div>
                        <div class="column">
                            {{ $haber->summary }}
                        </div>
                    </div>

                </div>
                @endif
                <div class="text">
                    <span class="icons" style="padding-top:0">
                        <a target="blank" href="https://www.facebook.com/sharer/sharer.php?u={{ $haber->url() }}"><i class="fa fa-facebook"></i></a> <br>
                        <a target="blank" href="http://twitter.com/share?url={{ $haber->url() }}"><i class="fa fa-twitter"></i></a>
                    </span>
                    {!! $haber->body !!}
                </div>
            </div>
            <div class="column">
                @include('layouts.sidebar')
            </div>
        </div>
    </div>
</div>
@stop

@section('js')
<script src='https://vjs.zencdn.net/7.5.5/video.js'></script>
<script>
    $(".openVideo").click(function() {
        $(".videoModal").addClass("is-active");  
    });

    $(".closeVideo").click(function() {
        $(".modal").removeClass("is-active");
        $("#haberVideo").trigger('pause');
    });

    $(".openPhotos").click(function() {
        $(".photosModal").addClass("is-active");  
    });

    $(".closePhotos").click(function() {
        $(".photosModal").removeClass("is-active");
    });
</script>
@stop
