@extends('layouts.app')

@section('css')
    <link rel="stylesheet" href="{{ asset('css/lightgallery.min.css') }}">
@endsection

@section('content')
<div id="pageTitle">
    <div class="container">
        <div class="level">
            <div class="level-left">
                <h1>Galeriler</h1>
            </div>
            <div class="level-right">
                <div class="links">
                    <a href="#">
                        <i class="fa fa-home"></i> Anasayfa
                    </a>
                    <i class="fa fa-angle-right"></i>
                    <a href="#">
                        Galeriler
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="galeri">
    <div class="container">
        <div class="my-gallery" itemscope itemtype="http://schema.org/ImageGallery">
        <div id="lightgallery">
            @foreach(json_decode($galery->photos) as $photo)
                <a class="" style=" margin-right:-5px;" href="{{ asset("storage/".$photo) }}">
                    <img style="width:25%;" src="{{ Voyager::image($galery->getThumbnail($photo, 'kare')) }}" />
                </a>
            @endforeach
        </div>
    </div>
    </div>
</div>

@endsection

@section('js')
    <script src="{{ asset('js/lightgallery.min.js') }}"></script>
    <script>
        lightGallery(document.getElementById('lightgallery'), {
            preload: 1
        });
    </script>
@endsection
