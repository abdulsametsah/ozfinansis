@extends('layouts.app')

@section('content')
<div id="pageTitle">
    <div class="container">
        <div class="level">
            <div class="level-left">
                <h1>Galeriler</h1>
            </div>
            <div class="level-right">
                <div class="links">
                    <a href="#">
                        <i class="fa fa-home"></i> Anasayfa
                    </a>
                    <i class="fa fa-angle-right"></i>
                    <a href="#">
                        Galeriler
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="galeriler">
    <div class="container">
        <div class="galeriler">
            <div class="columns">
                @foreach($galeriler as $galeri)
                    <div class="column is-3">
                        <div class="galeri">
                            <a href="{{ $galeri->url() }}">
                                <img src="{{ Voyager::image($galeri->thumbnail('kare', 'cover')) }}" alt="">
                                <div class="info">
                                    <div class="name">{{ $galeri->name }}</div>
                                    <div class="tarih">{{ date('d.m.Y', strtotime($galeri->created_at)) }}</div>
                                    <i class="fa fa-search"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        @if ($galeriler->lastPage() > 1)
            {!! $galeriler->links() !!}
        @endif
    </div>
</div>
@endsection
