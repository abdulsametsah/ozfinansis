@extends('layouts.app')
@section('content')


<div id="pageTitle">
    <div class="container">
        <div class="level">
            <div class="level-left">
                <h1>Kurumsal <span>/ Yönetim Kurulu</span></h1>
            </div>
            <div class="level-right">
                <div class="links">
                    <a href="{{ url('/') }}">
                        <i class="fa fa-home"></i> Anasayfa
                    </a>
                    <i class="fa fa-angle-right"></i>
                    <a href="{{ url('yonetim-kurulu') }}">
                        Yönetim Kurulu
                    </a>
                    <i class="fa fa-angle-right"></i>
                    <a href="#">
                        {{ $kisi->name }}
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="yonetimKurulu">
    <div class="container">
        <div class="single">
            <div class="columns">
                <div class="column is-6">
                    <img src="{{ Voyager::Image($kisi->image) }}" alt="">
                </div>
                <div class="column">
                    <h1>{{ $kisi->name }} <span>({{ $kisi->kurum }})</span></h1>
                    <div class="gorev">{{ $kisi->gorev }}</div>
                    <div class="social">
                        <a href="{{ $kisi->facebook }}"><i class="fa fa-facebook"></i></a>
                        <a href="{{ $kisi->twitter }}"><i class="fa fa-twitter"></i></a>
                        <a href="{{ $kisi->linkedin }}"><i class="fa fa-linkedin"></i></a>
                    </div>
                    <div class="bio">
                        {!! $kisi->bio !!}
                    </div>
                </div>
            </div>
        </div>

        <div id="page_title2">
            <h1 style="font-size:17px">YÖNETİM KURULU</h1>
        </div>
        <div class="columns is-multiline">
            @foreach($kisiler as $kisi)
            <div class="column is-one-quarter">
                <a href="{{ $kisi->url() }}">
                    <div class="kisi">
                        <img src="{{ Voyager::Image($kisi->thumbnail('kare')) }}">
                        <div class="info">
                            <div class="name">
                                {{ $kisi->name }} <span>({{ $kisi->kurum }})</span>
                            </div>
                            <div class="gorev">
                                {{ $kisi->gorev }}
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            @endforeach
        </div>
    </div>
</div>


@stop
