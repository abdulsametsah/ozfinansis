@extends('layouts.app')

@section('content')


<div id="pageTitle">
    <div class="container">
        <div class="level">
            <div class="level-left">
                <h1>Kurumsal <span>/ Yönetim Kurulu</span></h1>
            </div>
            <div class="level-right">
                <div class="links">
                    <a href="#">
                        <i class="fa fa-home"></i> Anasayfa
                    </a>
                    <i class="fa fa-angle-right"></i>
                    <a href="#">
                        Kurumsal
                    </a>
                    <i class="fa fa-angle-right"></i>
                    <a href="#">
                        Yönetim Kurulu
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div id="page_title2">
        <h1>Yönetim Kurulu</h1>
        <div class="desc">
            Öz Finans-İş Sendikası Yönetim Kurulu
        </div>
    </div>
</div>


<div id="yonetimKurulu">
    <div class="container">
        <div class="columns is-multiline">
            @foreach($kisiler as $kisi)
            <div class="column is-one-quarter">
                <a href="{{ $kisi->url() }}">
                    <div class="kisi">
                        <img src="{{ Voyager::Image($kisi->thumbnail('kare')) }}">
                        <div class="info">
                            <div class="name">
                                {{ $kisi->name }} <span>({{ $kisi->kurum }})</span>
                            </div>
                            <div class="gorev">
                                {{ $kisi->gorev }}
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            @endforeach
        </div>
    </div>
</div>


@stop
