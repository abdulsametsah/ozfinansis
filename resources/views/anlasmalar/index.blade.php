@extends('layouts.app')

@section('content')
<div id="pageTitle">
    <div class="container">
        <div class="level">
            <div class="level-left">
                <h1>
                    Anlaşmalar
                </h1>
            </div>
            <div class="level-right">
                <div class="links">
                    <a href="{{ url('/') }}">
                        <i class="fa fa-home"></i> Anasayfa
                    </a>
                    <i class="fa fa-angle-right"></i>
                    <a href="#">
                        Anlaşmalar
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="duyurular">
    <div class="container">
        <div class="duyurular2">
            <div class="columns is-multiline">
                
                @foreach($anlasmalar as $anlasma)
                    <div class="column is-3">
                        <div class=" duyuru">
                            <a href="{{ $anlasma->url() }}">
                                <img src="{{ Voyager::image($anlasma->thumbnail('kare', 'image')) }}" alt="">
                                <div class="info">
                                    <div class="title">{{ $anlasma->name }}</div>
                                    <p>{{ $anlasma->summary }}</p>
                                </div>
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        @if ($anlasmalar->lastPage() > 1)
            {!! $anlasmalar->links() !!}
        @endif
    </div>
</div>
@endsection
