@extends('layouts.app')

@section('content')
<div id="pageTitle">
    <div class="container">
        <div class="level">
            <div class="level-left">
                <h1>Anlaşmalar </h1>
            </div>
            <div class="level-right">
                <div class="links">
                    <a href="{{ url('/') }}">
                        <i class="fa fa-home"></i> Anasayfa
                    </a>
                    <i class="fa fa-angle-right"></i>
                    <a href="{{ url('/anlasmalar') }}">
                        Anlaşmalar
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="haber">
    <div class="container">
        <div class="columns">
            <div class="column is-8">
                <h1 class="title">{{ $anlasma->name }}</h1>
                <img src="{{ Voyager::Image($anlasma->image) }}" alt=""> <br><br>
                {!! $anlasma->body !!}
            </div>
            <div class="column">
                @include('layouts.sidebar')
            </div>
        </div>
    </div>
</div>
@stop
