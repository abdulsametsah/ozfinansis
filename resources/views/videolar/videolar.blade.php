@extends('layouts.app')

@section('content')
<div id="pageTitle">
    <div class="container">
        <div class="level">
            <div class="level-left">
                <h1>Videolar</h1>
            </div>
            <div class="level-right">
                <div class="links">
                    <a href="#">
                        <i class="fa fa-home"></i> Anasayfa
                    </a>
                    <i class="fa fa-angle-right"></i>
                    <a href="#">
                        Videolar
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="galeriler">
    <div class="container">
        <div class="galeriler">
            <div class="columns">
                @foreach($videos as $video)
                    <div class="column is-3">
                        <div class="galeri">
                            <a href="{{ $video->url() }}">
                                <img src="{{ Voyager::image($video->thumbnail('kare', 'image')) }}" alt="">
                                <div class="info">
                                    <div class="name">{{ $video->title }}</div>
                                    <div class="tarih">{{ date('d.m.Y', strtotime($video->created_at)) }}</div>
                                    <i class="fa fa-search"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        @if ($videos->lastPage() > 1)
            {!! $videos->links() !!}
        @endif
    </div>
</div>
@endsection
