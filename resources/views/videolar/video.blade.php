@extends('layouts.app')

@section('css')
    <link href="https://vjs.zencdn.net/7.5.5/video-js.css" rel="stylesheet">
@endsection

@section('content')
<div id="pageTitle">
    <div class="container">
        <div class="level">
            <div class="level-left">
                <h1>Videolar <span>/ {{ $video->title }}</span></h1>
            </div>
            <div class="level-right">
                <div class="links">
                    <a href="#">
                        <i class="fa fa-home"></i> Anasayfa
                    </a>
                    <i class="fa fa-angle-right"></i>
                    <a href="#">
                        Videolar
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="galeriler">
    <div class="container">
        <div class="galeriler">
            <div class="columns">
                <div class="column is-8">
                    <video  id='my-video' class='video-js' controls preload='auto' width='600' height='360'
                      poster='{{ Voyager::image($video->image) }}' data-setup='{}'>
                        <source src='{{ asset('storage/' . json_decode($video->videos)[0]->download_link) }}' type='video/mp4'>
                    </video>
                </div>
                <div class="column">
                    @include('layouts.sidebar')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script src='https://vjs.zencdn.net/7.5.5/video.js'></script>
@endsection
