@extends('layouts.app')

@section('content')
<div id="pageTitle">
    <div class="container">
        <div class="level">
            <div class="level-left">
                <h1>Yayınlar</h1>
            </div>
            <div class="level-right">
                <div class="links">
                    <a href="#">
                        <i class="fa fa-home"></i> Anasayfa
                    </a>
                    <i class="fa fa-angle-right"></i>
                    <a href="#">
                        Yayınlar
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="duyurular">
    <div class="container">
        <div class="duyurular2">
            <div class="columns is-multiline">
                @foreach($yayinlar as $yayin)
                    <div class="column is-3">
                        <div class=" duyuru">
                            <a target="_blank" href="{{ $yayin->url() }}">
                                <img src="{{ Voyager::image($yayin->thumbnail('kare', 'image')) }}" alt="">
                                <div class="tarih">
                                    <span>{{ turkcetarih_formati('d', $yayin->created_at) }}</span>
                                    <div>{{ turkcetarih_formati('M', $yayin->created_at) }}</div>
                                </div>
                                <div class="info">
                                    <div class="title">{{ $yayin->name }}</div>
                                    <p>Yayını indirmek için tıklayınız.</p>
                                </div>
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        @if ($yayinlar->lastPage() > 1)
            {!! $yayinlar->links() !!}
        @endif
    </div>
</div>
@endsection
