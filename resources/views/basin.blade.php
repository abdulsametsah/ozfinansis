@extends('layouts.app')

@section('content')
<div id="pageTitle">
    <div class="container">
        <div class="level">
            <div class="level-left">
                <h1>
                    Basında Biz
                </h1>
            </div>
            <div class="level-right">
                <div class="links">
                    <a href="{{ url('/') }}">
                        <i class="fa fa-home"></i> Anasayfa
                    </a>
                    <i class="fa fa-angle-right"></i>
                    <a href="#">
                        Basında Biz
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="duyurular">
    <div class="container">
        <div class="duyurular2">
            <div class="columns is-multiline">

                @foreach($basindakiler as $basin)
                    <div class="column is-3">
                        <div class="duyuru" style="cursor: pointer;" data-photo="{{ Voyager::Image($basin->photo) }}">
                            <img src="{{ Voyager::image($basin->thumbnail('kare', 'photo')) }}" alt="">
                            <div class="tarih">
                                <span>{{ turkcetarih_formati('d', $basin->created_at) }}</span>
                                <div>{{ turkcetarih_formati('M', $basin->created_at) }}</div>
                            </div>
                            <div class="info">
                                <div class="title">{{ $basin->name }}</div>
                                <p>Fotoğrafı görmek için tıklayınız.</p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        @if ($basindakiler->lastPage() > 1)
            {!! $basindakiler->links() !!}
        @endif
    </div>
</div>
    
<div id="imageView">
    <div class="content" id="imageViewContent">
        <div class="close">
            <i class="fa fa-times"></i>
        </div>
        <img src="http://localhost:8000/storage/basins/July2019/3ODToMpE2e5TbN7pud3L.jpg" alt="">
    </div>
</div>
@endsection

@section('js')
    <script>
        $(".duyurular2 .duyuru").click(function (e) {
            let url = $(this).data('photo');
            $("#imageView img").attr('src', url);
            $("#imageView").show();
        });

        $("#imageView .close").click(function () {
            $("#imageView").hide();
        });

        $("#imageView").click(function (e) {
            if ($(e.target).attr('id') == "imageView" || $(e.target).attr('id') == "imageViewContent") {
                $("#imageView").hide();
            }
        });
    </script>
@stop