@extends('layouts.app')


@section('content')
<div id="pageTitle">
    <div class="container">
        <div class="level">
            <div class="level-left">
                <h1>Başkandan Mesajlar</span></h1>
            </div>
            <div class="level-right">
                <div class="links">
                    <a href="#">
                        <i class="fa fa-home"></i> Anasayfa
                    </a>
                    <i class="fa fa-angle-right"></i>
                    <a href="#">
                        Başkandan Mesajlar
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="galeriler">
    <div class="container">
        <div class="galeriler">
            <div class="columns">
                <div class="column is-8">
                    <table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
                        @foreach ($mesajlar as $mesaj)
                        <tr>
                            <td><a href="{{ $mesaj->url() }}">{{ $mesaj->title }}</a></td>
                            <td>{{ turkcetarih_formati('d F Y, l', $mesaj->created_at) }}</td>
                        </tr>    
                        @endforeach
                        
                    </table>
                    @if ($mesajlar->lastPage() > 1)
                        {!! $mesajlar->links() !!}
                    @endif
                </div>
                <div class="column">
                    @include('layouts.sidebar')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
