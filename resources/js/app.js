window.Vue = require('vue');


let HomeHaberlerSlider = new Vue({
    el: '#HomeHaberlerSlider',
    name: "HomeHaberlerSlider",
    data: {
        currentHaber: 1,
        haberSayisi: 0,
    },
    mounted () {
        this.haberSayisi = parseInt(document.getElementById("home_haberler_count").value)
    },
    watch: {
        currentHaber (val) {
            //alert(val);
        }
    }
});

let anlasmalar = new Vue({
    el: '.anlasmalar',
    name: 'Anlasmalar',
    data: {
        start: 0,
        end: 2,
        count: 0,
        showNext: true,
        showBefore: false,
    },
    mounted () {
        this.count = parseInt(document.getElementById("anlasmalar_count").value);
        if (this.count < 3) {
            this.showNext = false;
        }
    },
    methods: {
        goster (index) {
            return index >= this.start && index <= this.end;
        },
        next() {
            if (this.end != this.count-1) {
                this.start++;
                this.end++;
            }
        },
        before() {
            if (this.start != 0) {
                this.start--;
                this.end--;
            }
        }
    },
    watch: {
        start(val) {
            if (val == 0) {
                this.showBefore = false;
            } else {
                this.showBefore = true;
            }
        },
        end(val) {
            if (val == this.count - 1) {
                this.showNext = false;
            } else {
                this.showNext = true;
            }
        }
    }
});

let digerHaberler = new Vue({
    el: '.other_news',
    name: 'DigerHaberler',
    data: {
        show: 'diger'
    }
});

let duyurular = new Vue({
    el: '#duyurularVue',
    name: 'Duyurular',
    data: {
        displayType: 'flow',
        duyurular: [
            {
                tarih: '12 Haziran 2019, Salı',
                image: 'https://picsum.photos/id/237/700',
                title: 'Lorem ipsum dolor sit amet, consectetur.',
                text: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. ' +
                    'Modi reprehenderit impedit quo reiciendis, ducimus, voluptatibus aliquam quibusdam.',
                url: '#',
            },
            {
                tarih: '12 Haziran 2019, Salı',
                image: 'https://picsum.photos/id/237/700',
                title: 'Lorem ipsum dolor sit amet, consectetur.',
                text: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. ' +
                    'Modi reprehenderit impedit quo reiciendis, ducimus, voluptatibus aliquam quibusdam.',
                url: '#',
            },
        ]
    },
    mounted () {
        this.setHeight();
    },
    methods: {
        setHeight() {
            let height = $("#duyurular .duyurular").height() + 47;
            $("#duyurular .cubuk").css('height', (height-70) + "px");
        },
        next () {
            this.duyurular.push(
                {
                    tarih: '12 Haziran 2019, Salı',
                    image: 'https://picsum.photos/id/237/700',
                    title: 'Lorem ipsum dolor sit amet, consectetur.',
                    text: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. ' +
                        'Modi reprehenderit impedit quo reiciendis, ducimus, voluptatibus aliquam quibusdam.',
                    url: '#',
                }
            );
        },
    },
    watch: {
        duyurular () {
            setTimeout(this.setHeight() ,1000);
        },
        displayType () {
            console.log("cccc");
            setTimeout(this.setHeight() ,1000);
        }
    }
});
