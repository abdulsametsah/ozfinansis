<?php $__env->startSection('content'); ?>
<div id="duyurularVue">

    <div id="pageTitle">
        <div class="container">
            <div class="level">
                <div class="level-left">
                    <h1>Duyurular</h1>
                </div>
                <div class="level-right">
                    <div class="links">
                        <a href="#">
                            <i class="fa fa-home"></i> Anasayfa
                        </a>
                        <i class="fa fa-angle-right"></i>
                        <a href="#">
                            Duyurular
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div id="page_title2">
            <h1>Duyurular</h1>
            <div class="desc">
                Öz Finans-İş Sendikası Duyurular
            </div>
            <div class="turButtons">
                <a href="?type=flow" style="color:#000;"><i class="fa fa-list"></i></a>
                <a href="?type=grid" style="color:#000;"><i class="fa fa-th"></i></a>
            </div>
        </div>
    </div>

    <div id="duyurular">

        <div class="container">
            <?php if($type == 'flow'): ?>
                <div class="cubuk"></div>
                <div class="duyurular">
                    <span>
                        <?php $i = 1; ?>
                        <?php $__currentLoopData = $duyurular; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $duyuru): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php $i++; ?>
                            <article :class="'duyuru ' + (<?php echo e($i); ?>%2 == 0 ? 'duyuru-sol':'')">
                                <a href="<?php echo e($duyuru->url()); ?>">
                                    <div class="tarih">
                                        <span><?php echo e(turkcetarih_formati('d F Y, l', $duyuru->created_at)); ?></span>
                                    </div>
                                    <div class="body">
                                        <div class="columns">
                                            <div class="column is-8">
                                                <h2 style="color:#000"><?php echo e($duyuru->title); ?></h2>
                                                <p>
                                                    <?php echo e($duyuru->summary); ?>

                                                </p>
                                            </div>
                                            <div class="column">
                                                <img src="<?php echo e(Voyager::image($duyuru->thumbnail('kare', 'image'))); ?>" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="nokta"><span></span></div>
                                    <div class="clear"></div>
                                </a>
                            </article>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <div class="clear"></div>
                        <br>
                        <?php if($duyurular->lastPage() > 1): ?>
                        <div class="more" style="display:none;">
                            <i @click="next" class="fa fa-angle-double-down"></i>
                        </div>
                        <?php endif; ?>
                    </span>
                </div>
            <?php else: ?>
                <div class="duyurular2">
                    <div class="columns is-multiline">
                        <?php $__currentLoopData = $duyurular; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $duyuru): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="column is-3">
                                <div class="duyuru">
                                    <a href="<?php echo e($duyuru->url()); ?>">
                                        <img src="<?php echo e(Voyager::image($duyuru->thumbnail('kare', 'image'))); ?>" alt="">
                                        <div class="tarih">
                                            <span><?php echo e(turkcetarih_formati('d', $duyuru->created_at)); ?></span>
                                            <div><?php echo e(turkcetarih_formati('M', $duyuru->created_at)); ?></div>
                                        </div>
                                        <div class="info">
                                            <div class="title"><?php echo e($duyuru->title); ?></div>
                                            <p><?php echo e($duyuru->summary); ?></p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>

    </div>

    <div class="container">


        <?php echo $duyurular->links(); ?>

    </div>
</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\asame\Desktop\Projects\clients\ozfinansis\resources\views/duyurular.blade.php ENDPATH**/ ?>