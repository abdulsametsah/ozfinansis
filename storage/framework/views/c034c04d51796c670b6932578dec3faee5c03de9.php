<?php $__env->startSection('css'); ?>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<!-- HOME_PRESIDENT-->
<div class="home-president">
    <div class="container">
        <div class="left">
            <!-- PRESIDENT -->
            <div class="president">
                <div class="top">
                    <strong>Ahmet Eroğlu</strong>
                    <br>
                    Genel Başkan
                </div>
                <div class="bottom">
                    <a href="<?php echo e(url('/baskandan-mesajlar')); ?>" style="color:#fff;">Başkandan Mesaj</a>
                </div>

                <img src="<?php echo e(asset('img/baskan.png')); ?>" alt="">
            </div>
            <!-- PRESIDENT END -->
            <!-- LINKLER -->
            <div class="linkler">
                <div class="link">
                    <a href="<?php echo e(url('fotograflar')); ?>">
                        <i class="fa fa-photo"></i>
                        <span>FOTO GALERİ</span>
                    </a>
                </div>
                <div class="link">
                    <a href="<?php echo e(url('videolar')); ?>">
                        <i class="fa fa-film"></i>
                        <span>VİDEO GALERİ</span>
                    </a>
                </div>
                <div class="link">
                    <a href="<?php echo e(url('yayinlar')); ?>">
                        <i class="fa fa-archive"></i>
                        <span>YAYINLAR</span>
                    </a>
                </div>
            </div>
            <!-- LINKLER END -->
        </div>

        <div class="right">
            <div class="slider">
                <div class="owl-carousel owl-theme">
                    <?php $__currentLoopData = $slider_haberler; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $haber): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="item">
                        <div class="haber">
                            <a href="<?php echo e($haber->url()); ?>">
                                <img src="<?php echo e(Voyager::Image($haber->thumbnail('slider'))); ?>" alt="">
                                <div class="info">
                                    <?php echo e($haber->title); ?>

                                </div>
                            </a>
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
        </div>

        <div class="clear"></div>
    </div>
</div>
<!-- HOME_PRESIDENT END-->

<!-- HOME_LINKLER -->
<div class="container">
    <div class="home-linkler">
        <div class="columns">
            <!-- item -->
            <div class="column item">
                <a href="<?php echo e(url('basinda-biz')); ?>">
                    <div class="columns">
                        <div class="column is-3">
                            <i class="fa fa-map-o"></i>
                        </div>
                        <div class="column">
                            <span>Basında Biz</span> <br>
                            BASINDA ÖZ FİNANS-İŞ
                        </div>
                    </div>
                </a>
            </div>
            <!-- item end -->
            <!-- item -->
            <div class="column item">
                <a href="<?php echo e(url('duyurular')); ?>">
                    <div class="columns">
                        <div class="column is-3">
                            <i class="fa fa-bullhorn"></i>
                        </div>
                        <div class="column">
                            <span>Güncel Duyurular</span> <br>
                            DUYURULAR
                        </div>
                    </div>
                </a>
            </div>
            <!-- item end -->
            <!-- item -->
            <div class="column item">
                <a href="<?php echo e(url('haberler')); ?>">
                    <div class="columns">
                        <div class="column is-3">
                            <i class="fa fa-newspaper-o"></i>
                        </div>
                        <div class="column">
                            <span>Güncel Haberler</span> <br>
                            HABERLER
                        </div>
                    </div>
                </a>
            </div>
            <!-- item end -->
            <!-- item -->
            <div class="column item">
                <a href="<?php echo e(url('forum')); ?>">
                    <div class="columns">
                        <div class="column is-3">
                            <i class="fa fa-comments-o"></i>
                        </div>
                        <div class="column">
                            <span>Sesinizi Duyurun</span> <br>
                            ÖZ FİNANS-İŞ FORUM
                        </div>
                    </div>
                </a>
            </div>
            <!-- item end -->
        </div>
    </div>
</div>
<!-- HOME_LINKLER END -->

<!-- HOME BOTTOM -->
<div class="container">
    <div class="home-bottom">
        <div class="columns">
            <!-- UYE OL -->
            <div class="column is-4">
                <div class="uyeol">
                    Sendikamıza üye olun
                    emeğinize sahip çıkın

                    <a href="#" class="button">e-Devlet'ten üye olmak için tıklayın</a>
                </div>
            </div>
            <!-- UYE OL END-->
            <!-- HOME HABERLER -->
            <div class="column">
                <div id="HomeHaberlerSlider" v-cloak class="home-haberler">
                    <div class="home_haberler_title">
                        <div class="level">
                            <div class="level-left">
                                <h2>ÖZ FİNANS-İŞ HABERLER</h2>
                            </div>
                            <div class="level-right">
                                <a href="#"> Tüm Haberler </a>
                            </div>
                        </div>
                    </div>
                    <div class="home_haberler_content">
                        <input type="hidden" id="home_haberler_count" value="2">
                        <?php $i = 1; ?>
                        <?php $__currentLoopData = $alt_haberler; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $haber): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="haber" v-if="currentHaber == <?php echo e($i++); ?>">
                                <div class="columns">
                                    <div class="column is-7">
                                        <a href="<?php echo e($haber->url()); ?>">
                                            <img src="<?php echo e(Voyager::image($haber->thumbnail('500x300', 'image'))); ?>" alt="">
                                        </a>
                                    </div>
                                    <div class="column">
                                        <a href="<?php echo e($haber->url()); ?>">
                                            <h3><?php echo e($haber->title); ?></h3>
                                            <div class="date"><?php echo e(turkcetarih_formati('d F Y, l', $haber->created_at)); ?></div>
                                            <p>
                                                <?php echo e($haber->summary); ?>

                                            </p>
                                        </a>

                                        <div class="dots">
                                            <span v-for="i in haberSayisi" @click="currentHaber = i"
                                                :class="currentHaber == (i) ? 'aktif':''"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>
            </div>
            <!-- HOME HABERLER END -->
        </div>
    </div>
</div>
<!-- HOME BOTTOM END -->
<!-- ANLASMALAR -->
<div class="anlasmalar">
    <div class="container">
        <div class="top">
            <div class="columns">
                <div class="column is-4">
                    <h1>KURUMSAL<br><span>ANLAŞMALARIMIZ</span></h1>
                </div>
                <div class="column is-5">
                    <p>
                        Üyelerimiz için çok özel anlaşmalar. <br>
                        Öz Finans-İş Sendikası üyelerimiz çok özel ayrıcalıklardan faydalanıyor
                    </p>
                </div>
                <div class="column">
                    <div class="buttons">
                        <button class="button" @click="before" v-if="showBefore"><i class="fa fa-arrow-left"></i></button>
                        <button class="button" @click="next" v-if="showNext"><i class="fa fa-arrow-right"></i></button>
                    </div>
                </div>
            </div>
        </div>

        <div class="photos">
            <input type="hidden" id="anlasmalar_count" value="<?php echo e(count($anlasmalar)); ?>">
            <div class="columns">
                <?php $i=0; ?>
                <?php $__currentLoopData = $anlasmalar; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $anlasma): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="column is-4" v-if="goster(<?php echo e($i++); ?>)">
                    <a href="<?php echo e($anlasma->url()); ?>">
                        <img src="<?php echo e(Voyager::Image($anlasma->thumbnail('500x300'))); ?>" alt="">
                    </a>
                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        </div>
    </div>
</div>
<!-- ANLASMALAR END -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
    <script>
        $('.owl-carousel').owlCarousel({
            loop:true,
            margin:10,
            nav:false,
            dots: false,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1000:{
                    items:1
                }
            }
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\asame\Desktop\Projects\clients\ozfinansis\resources\views/home.blade.php ENDPATH**/ ?>