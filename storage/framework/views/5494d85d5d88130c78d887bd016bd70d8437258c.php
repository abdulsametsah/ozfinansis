<!-- PRESIDENT -->
<div class="president">
    <div class="top">
        <strong>Ahmet Eroğlu</strong>
        <br>
        Genel Başkan
    </div>
    <div class="bottom">
        <span class="icons">
            <i class="fa fa-facebook"></i>
            <i class="fa fa-twitter"></i>
        </span>
        <a style="color:#fff" href="<?php echo e(url('/baskandan-mesajlar')); ?>">Başkandan Mesaj</a>
    </div>

    <img src="<?php echo e(asset('img/baskan.png')); ?>" alt="">
</div>
<!-- PRESIDENT END -->
<!-- OTHER NEWS -->
<div class="other_news">
    <div class="top">
        <div class="columns">
            <div class="column" @click="show='diger'">
                <h2 :class="show=='diger' ? 'active':''">DİĞER HABERLER</h2>
            </div>
            <div class="column" @click="show='son'">
                <h2 :class="show=='son' ? 'active':''">SON EKLENENLER</h2>
            </div>
        </div>
    </div>
    <div class="body">
        <div class="haberler">
            <!-- diğer -->
            <ul v-if="show == 'diger'">
                <?php $__currentLoopData = \App\Haber::inRandomOrder()->where('lang', app()->getLocale())->where('basin_aciklamasi', false)->limit(3)->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $haber): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li>
                    <div class="columns">
                        <div class="column is-5">
                            <a href="<?php echo e($haber->url()); ?>"><img src="<?php echo e(Voyager::image($haber->thumbnail('127x63', 'image'))); ?>" alt=""></a>
                        </div>
                        <div class="column">
                            <a href="<?php echo e($haber->url()); ?>">
                                <h3><?php echo e($haber->title); ?></h3>
                                <div class="date">
                                    <i class="fa fa-clock-o"></i>
                                    10 Haziran 2018 - 08:15
                                </div>
                            </a>
                        </div>
                    </div>
                </li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
            <!-- diğer end -->
            <!-- son eklenen -->
            <ul v-if="show == 'son'">
                <?php $__currentLoopData = \App\Haber::latest()->where('lang', app()->getLocale())->where('basin_aciklamasi', false)->limit(3)->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $haber): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li>
                    <div class="columns">
                        <div class="column is-5">
                            <a href="<?php echo e($haber->url()); ?>"><img src="<?php echo e(Voyager::image($haber->thumbnail('127x63', 'image'))); ?>" alt=""></a>
                        </div>
                        <div class="column">
                            <a href="<?php echo e($haber->url()); ?>">
                                <h3><?php echo e($haber->title); ?></h3>
                                <div class="date">
                                    <i class="fa fa-clock-o"></i>
                                    10 Haziran 2018 - 08:15
                                </div>
                            </a>
                        </div>
                    </div>
                </li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
            <!-- son eklenen end -->
        </div>
    </div>
    <div class="bottom">
        <a href="<?php echo e(url('haberler')); ?>">TÜM HABERLER</a>
    </div>
</div>
<!-- OTHER NEWS END -->
<!-- OTHER NEWS -->
<div class="other_news2">
    <div class="top">
        <div class="columns">
            <div class="column">
                <h2 :class="show=='diger' ? 'active':''">İLGİLİ KATEGORİLER</h2>
            </div>
        </div>
    </div>
    <div class="body">
        <div class="menu">
            <ul>
                <li><a href="<?php echo e(url('basin-aciklamalari')); ?>">BASIN AÇIKLAMALARI</a></li>
                <li><a href="<?php echo e(url('duyurular')); ?>">DUYURULAR</a></li>
                <li><a href="<?php echo e(url('haberler')); ?>">HABERLER</a></li>
            </ul>
        </div>
    </div>
</div>
<br><br>
<!-- OTHER NEWS END -->
<?php /**PATH C:\Users\asame\Desktop\Projects\clients\ozfinansis\resources\views/layouts/sidebar.blade.php ENDPATH**/ ?>