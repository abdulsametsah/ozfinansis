<?php $__env->startSection("content"); ?>
    <div id="pageTitle">
        <div class="container">
            <div class="level">
                <div class="level-left">
                    <h1>
                        Forum
                        <span>/ Giriş Yap</span>
                    </h1>
                </div>
                <div class="level-right">
                    <div class="links">
                        <a href="#">
                            <i class="fa fa-home"></i> Anasayfa
                        </a>
                        <i class="fa fa-angle-right"></i>
                        <a href="#">
                            Forum
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="forum-register">
            <form action="" id="step1">
                <label for="email">E-Posta Adresiniz</label>
                <input type="text" id="email" name="email">
                <label for="password">Şifreniz</label>
                <input type="password" id="password" name="password">
                <button id="check-button">Giriş Yap</button>
            </form>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection("js"); ?>
    <script>
        const checkButton = document.getElementById("check-button");
        checkButton.addEventListener("click", (e) => {
            e.preventDefault();

            const email = document.getElementById("email").value;
            const password = document.getElementById("password").value;

            $.ajax({
                url: '<?php echo e(url(app()->getLocale() . "/forum/login")); ?>',
                method: "post",
                data: {
                    _token: "<?php echo e(csrf_token()); ?>",
                    email, password
                },
                success: (response) => {
                    if (response.status === "error") {
                        return Swal.fire(response.message);
                    }else {
                        window.location.href = "<?php echo e(url(app()->getLocale() . '/forum')); ?>";
                    }
                }
            });
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make("layouts.app", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\asame\Desktop\Projects\clients\ozfinansis\resources\views/forum/login.blade.php ENDPATH**/ ?>