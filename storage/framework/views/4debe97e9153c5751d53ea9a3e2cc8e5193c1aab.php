<?php $__env->startSection('js'); ?>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script>
        function onSubmit(token) {
            document.getElementById("iletisimForm").submit();
        }
    </script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div id="duyurularVue">

    <div id="pageTitle" style="margin-bottom:0;">
        <div class="container">
            <div class="level">
                <div class="level-left">
                    <h1>Kurumsal <span>/ İletişim</span></h1>
                </div>
                <div class="level-right">
                    <div class="links">
                        <a href="#">
                            <i class="fa fa-home"></i> Anasayfa
                        </a>
                        <i class="fa fa-angle-right"></i>
                        <a href="#">
                            İletişim
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <iframe src="https://maps.google.com/maps?q=Beştepe Mahallesi Merhale Sokak No:8 Yenimahalle/Ankara&amp;
    iwloc=near&amp;output=embed" height="380" frameborder="0" style="border:0;width:100%; height: 340px !important" class="lazy-loaded"></iframe>

    <div id="iletisim">
        <div class="container">
            <div class="columns">
                <div class="column is-4">
                    <ul class="solMenu">
                        <li>
                            <div class="columns">
                                <div class="column is-10">
                                    <div class="title">ADRES</div>
                                    <p>
                                        Beştepe Mahallesi Merhale Sokak No:8 Yenimahalle/Ankara
                                    </p>
                                </div>
                                <div class="column icon">
                                    <i class="fa fa-map-marker"></i>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="columns">
                                <div class="column is-10">
                                    <div class="title">TELEFON</div>
                                    <p>
                                        0312 341 88 77 <br>
                                        0312 341 77 85 (FAX)
                                    </p>
                                </div>
                                <div class="column icon">
                                    <i class="fa fa-phone"></i>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="columns">
                                <div class="column is-10">
                                    <div class="title">E-POSTA</div>
                                    <p>
                                        bilgi@ozfinansis.org.tr
                                    </p>
                                </div>
                                <div class="column icon">
                                    <i class="fa fa-envelope" style="margin-top: 5px"></i>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="columns">
                                <div class="column">
                                    <div class="title">SOSYAL MEDYA</div>
                                    <p>
                                        <span class="sosyal">
                                            <a href="#"><i class="fa fa-facebook"></i></a>
                                            <a href="#"><i class="fa fa-twitter"></i></a>
                                            <a href="#"><i class="fa fa-linkedin"></i></a>
                                        </span>
                                    </p>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="column">
                    <div class="iletisimFormu">
                        <h2>İLETİŞİM FORMU</h2>
                        <?php if(session('error')): ?>
                            <div class="notification is-danger">
                              <?php echo e(session('error')); ?>

                            </div>
                        <?php endif; ?>
                        <?php if(session('success')): ?>
                            <div class="notification is-primary">
                              <?php echo e(session('success')); ?>

                            </div>
                        <?php endif; ?>
                        <form method="post" id="iletisimForm">
                            <?php echo csrf_field(); ?>
                            <input type="text" name="name" value="<?php echo e(old('name')); ?>" placeholder="Adınız Soyadınız" class="input">
                            <br>
                            <br>
                            <div class="columns">
                                <div class="column">
                                    <input type="text" value="<?php echo e(old('phone')); ?>" name="phone" placeholder="Telefon Numarınız" class="input">
                                </div>
                                <div class="column">
                                    <input type="text" value="<?php echo e(old('email')); ?>" name="email" placeholder="E-Posta Adresiniz" class="input">
                                </div>
                            </div>
                            <textarea name="text" placeholder="Mesajınız.." class="textarea"><?php echo e(old('text')); ?></textarea>

                            <button class="button g-recaptcha" data-sitekey="<?php echo e(env('GOOGLE_RECAPTCHA_KEY')); ?>" data-callback='onSubmit'>GÖNDER</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\asame\Desktop\Projects\clients\ozfinansis\resources\views/iletisim.blade.php ENDPATH**/ ?>