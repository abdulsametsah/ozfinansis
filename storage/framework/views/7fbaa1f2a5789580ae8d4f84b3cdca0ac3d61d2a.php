<?php $__env->startSection('content'); ?>
<div id="pageTitle">
    <div class="container">
        <div class="level">
            <div class="level-left">
                <h1>Duyuru</h1>
            </div>
            <div class="level-right">
                <div class="links">
                    <a href="#">
                        <i class="fa fa-home"></i> Anasayfa
                    </a>
                    <i class="fa fa-angle-right"></i>
                    <a href="#">
                        Duyurular
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="haber">
    <div class="container">
        <div class="columns">
            <div class="column is-8">
                <img src="<?php echo e(Voyager::image($duyuru->image)); ?>" style="max-width:100%" alt="">
                <br> 
                <h1><?php echo e($duyuru->title); ?></h1>
                <br><br>
                <?php echo $duyuru->body; ?>

            </div>
            <div class="column">
                <?php echo $__env->make('layouts.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\asame\Desktop\Projects\clients\ozfinansis\resources\views/duyuru.blade.php ENDPATH**/ ?>