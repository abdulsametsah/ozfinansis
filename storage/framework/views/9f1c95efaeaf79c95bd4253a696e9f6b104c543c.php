<div class="container">
    <?php if(!auth()->check()): ?>
        <div class="forum-user">
            <a href="<?php echo e(url(app()->getLocale() . "/forum/login")); ?>" class="btn">Giriş Yap</a>
            <a href="<?php echo e(url(app()->getLocale() . "/forum/register")); ?>" class="btn">Kayıt Ol</a>
        </div>
    <?php else: ?>
        <div class="forum-logged-user">
            <div class="user">
                <i class="fa fa-user"></i>
                Merhaba, <strong><?php echo e(auth()->user()->name); ?></strong>.
            </div>
        </div>
    <?php endif; ?>
</div>
<?php /**PATH C:\Users\asame\Desktop\Projects\clients\ozfinansis\resources\views/layouts/forumUser.blade.php ENDPATH**/ ?>