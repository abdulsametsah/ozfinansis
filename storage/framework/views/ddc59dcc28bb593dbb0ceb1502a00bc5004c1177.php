<?php $__env->startSection('content'); ?>
<div id="pageTitle">
    <div class="container">
        <div class="level">
            <div class="level-left">
                <h1>
                    Haberler
                    <?php if($q): ?>
                        <span>/ Ara: <?php echo e($q); ?></span>
                    <?php endif; ?>
                </h1>
            </div>
            <div class="level-right">
                <div class="links">
                    <a href="<?php echo e(url('/')); ?>">
                        <i class="fa fa-home"></i> Anasayfa
                    </a>
                    <i class="fa fa-angle-right"></i>
                    <a href="#">
                        Haberler
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="duyurular">
    <div class="container">
        <div class="duyurular2">
            <div class="columns is-multiline">
                
                <?php $__currentLoopData = $haberler; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $haber): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="column is-3">
                        <div class=" duyuru">
                            <a href="<?php echo e($haber->url()); ?>">
                                <img src="<?php echo e(Voyager::image($haber->thumbnail('kare', 'image'))); ?>" alt="">
                                <div class="tarih">
                                    <span><?php echo e(turkcetarih_formati('d', $haber->created_at)); ?></span>
                                    <div><?php echo e(turkcetarih_formati('M', $haber->created_at)); ?></div>
                                </div>
                                <div class="info">
                                    <div class="title"><?php echo e($haber->title); ?></div>
                                    <p><?php echo e($haber->summary); ?></p>
                                </div>
                            </a>
                        </div>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        </div>

        <br><br>
        <?php if($haberler->lastPage() > 1): ?>
            <?php echo $haberler->links(); ?>

        <?php endif; ?>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\asame\Desktop\Projects\clients\ozfinansis\resources\views/haberler/haberler.blade.php ENDPATH**/ ?>