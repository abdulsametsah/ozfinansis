<?php $__env->startSection('css'); ?>
    <link href="https://vjs.zencdn.net/7.5.5/video-js.css" rel="stylesheet">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div id="pageTitle">
    <div class="container">
        <div class="level">
            <div class="level-left">
                <h1>Haber</h1>
            </div>
            <div class="level-right">
                <div class="links">
                    <a href="#">
                        <i class="fa fa-home"></i> Anasayfa
                    </a>
                    <i class="fa fa-angle-right"></i>
                    <a href="#">
                        Kurumsal
                    </a>
                    <i class="fa fa-angle-right"></i>
                    <a href="#">
                        Yönetim Kurulu
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="haber">
    <div class="container">
        <div class="columns">
            <div class="column is-8">
                <div class="cover">
                    <img src="<?php echo e(Voyager::image($haber->image)); ?>" alt="">
                    <?php if($haber->video || $haber->photos): ?>
                    <div class="icons2">
                        <?php if($haber->video && isset(json_decode($haber->video)[0])): ?>
                        <div class="icon2 openVideo">
                            <i class="fa fa-play-circle"></i>
                            <span>VİDEO</span>
                        </div>
                        <?php endif; ?>
                        <?php if($haber->photos): ?>
                        <div class="icon2 openPhotos" style="background: #a3620a;">
                            <i class="fa fa-camera"></i>
                            <span>GALERİ</span>
                            
                        </div>
                        <?php endif; ?>
                    </div>
                    <?php endif; ?>
                </div>
                <div class="modal videoModal">
                    <div class="modal-background"></div>
                    <div class="modal-content">
                        <?php if($haber->video && isset(json_decode($haber->video)[0])): ?>
                        <video width="700" height="240" id="haberVideo" controls>
                            <source src='<?php echo e(asset('storage/' . json_decode($haber->video)[0]->download_link)); ?>' type='video/mp4'>
                        </video>
                        <?php endif; ?>
                    </div>
                    <button class="modal-close closeVideo is-large" aria-label="close"></button>
                </div>
                
                <div class="modal photosModal">
                    <div class="modal-background"></div>
                    <div class="modal-content">
                        <?php if($haber->photos): ?>
                            <?php $__currentLoopData = json_decode($haber->photos); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $photo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <img src="<?php echo e(asset('storage/' . $photo)); ?>" height="100" alt="">
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?>
                    </div>
                    <button class="modal-close closePhotos is-large" aria-label="close"></button>
                </div>
                <h1><?php echo e($haber->title); ?></h1>
                <div class="info">
                    <span>
                        <i class="fa fa-calendar-o"></i>
                        <?php echo e(turkcetarih_formati('d F Y, l', $haber->created_at)); ?>

                    </span>
                    <span>
                        <i class="fa fa-clock-o"></i>
                        <?php echo e(turkcetarih_formati('H:m', $haber->created_at)); ?>

                    </span>
                    <span>
                        <i class="fa fa-comment"></i>
                        <?php echo e(number_format($haber->comments)); ?>

                    </span>
                </div>
                <?php if($haber->summary): ?>
                <div class="summary">
                    <div class="columns">
                        <div class="column is-1" style="text-align:right;">
                            <i class="fa fa-quote-left"></i>
                        </div>
                        <div class="column">
                            <?php echo e($haber->summary); ?>

                        </div>
                    </div>

                </div>
                <?php endif; ?>
                <div class="text">
                    <span class="icons" style="padding-top:0">
                        <a target="blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo e($haber->url()); ?>"><i class="fa fa-facebook"></i></a> <br>
                        <a target="blank" href="http://twitter.com/share?url=<?php echo e($haber->url()); ?>"><i class="fa fa-twitter"></i></a>
                    </span>
                    <?php echo $haber->body; ?>

                </div>
            </div>
            <div class="column">
                <?php echo $__env->make('layouts.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<script src='https://vjs.zencdn.net/7.5.5/video.js'></script>
<script>
    $(".openVideo").click(function() {
        $(".videoModal").addClass("is-active");  
    });

    $(".closeVideo").click(function() {
        $(".modal").removeClass("is-active");
        $("#haberVideo").trigger('pause');
    });

    $(".openPhotos").click(function() {
        $(".photosModal").addClass("is-active");  
    });

    $(".closePhotos").click(function() {
        $(".photosModal").removeClass("is-active");
    });
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\asame\Desktop\Projects\clients\ozfinansis\resources\views/haberler/haber.blade.php ENDPATH**/ ?>