<?php $__env->startSection('content'); ?>
<div id="pageTitle">
    <div class="container">
        <div class="level">
            <div class="level-left">
                <h1>Forum</h1>
            </div>
            <div class="level-right">
                <div class="links">
                    <a href="#">
                        <i class="fa fa-home"></i> Anasayfa
                    </a>
                    <i class="fa fa-angle-right"></i>
                    <a href="#">
                        Kurumsal
                    </a>
                    <i class="fa fa-angle-right"></i>
                    <a href="#">
                        Yönetim Kurulu
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<?php echo $__env->make("layouts.forumUser", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<div id="forum_index">
    <div class="container">
        <table class="table question is-bordered is-striped is-narrow is-hoverable is-fullwidth">
            <tr>
                <th colspan="2"><a style="color: #222" href="<?php echo e($konu->forum()->url()); ?>"><?php echo e($konu->forum()->name); ?></a></th>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="konu_basligi">
                        <span>Konu:</span> <?php echo e($konu->name); ?>

                    </div>
                </td>
            </tr>

            <?php $__currentLoopData = $mesajlar; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $mesaj): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <td class="yazar">
                    <div>
                        <div class="name"><?php echo e($mesaj->user()->name); ?></div>
                        <div class="date"><?php echo e($mesaj->timeAgo()); ?></div>
                        <div class="online">online</div>
                    </div>
                </td>
                <td>
                    <div class="text">
                        <?php echo e($mesaj->content); ?>

                    </div>
                </td>
            </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

            <?php if(auth()->check()): ?>
            <tr>
                <td class="yazan"></td>
                <td>
                    <form action="" class="cevap_yaz">
                        <textarea name="body" id="hizliCevapContent" class="textarea"></textarea>
                        <button class="button" id="hizliCevap">HIZLI CEVAP YAZ</button>
                        <button class="button">DETAYLI CEVAP YAZ</button>
                    </form>
                </td>
            </tr>
            <?php endif; ?>
        </table>

        <?php echo $mesajlar->links(); ?>


        <br><br>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<script>
    $("#hizliCevap").click((e) => {
        e.preventDefault();

        let mesaj = $("#hizliCevapContent").val();

        if (mesaj.length > 0) {
            $.ajax({
                type: 'post',
                url : '<?php echo e(url(app()->getLocale() . "/forum/" . $mesaj->konu()->forum()->id . "/"  . $mesaj->konu()->id)); ?>',
                data: {
                    _token: '<?php echo e(csrf_token()); ?>',
                    mesaj: mesaj
                },
                success: (cevap) => {
                    Swal.fire({
                      icon: cevap.status,
                      text: cevap.message
                    }).then(() => {
                        if (cevap.status === "success") {
                            window.location.reload();
                        }
                    });
                }
            });
        }
    });
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\asame\Desktop\Projects\clients\ozfinansis\resources\views/forum/question.blade.php ENDPATH**/ ?>