<?php $__env->startSection('content'); ?>
    <div id="pageTitle">
        <div class="container">
            <div class="level">
                <div class="level-left">
                    <h1>Forum</h1>
                </div>
                <div class="level-right">
                    <div class="links">
                        <a href="#">
                            <i class="fa fa-home"></i> Anasayfa
                        </a>
                        <i class="fa fa-angle-right"></i>
                        <a href="#">
                            Kurumsal
                        </a>
                        <i class="fa fa-angle-right"></i>
                        <a href="#">
                            Yönetim Kurulu
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php echo $__env->make("layouts.forumUser", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

    <div id="forum_index">
        <div class="container">
            <?php if(auth()->check()): ?>
            <br>
            <div class="level">
                <div class="level-left"></div>
                <div class="level-right">
                    <a href="#" class="button is-danger">YENİ KONU AÇ</a>
                </div>
            </div>
            <br>
            <?php endif; ?>

            <table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
                <tr>
                    <th>Görevde Yükselme İle İlgili Konular</th>
                    <th style="text-align:center">MESAJ</th>
                    <th>SON MESAJ ZAMANI</th>
                </tr>
                <?php $__currentLoopData = $konular; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $konu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td>
                            <a href="<?php echo e(url(app()->getLocale() . "/forum/" . $forum->id . "/" . $konu->id)); ?>">
                                <div class="baslik"><?php echo e($konu->name); ?></div>
                                <div class="desc"><?php echo e($konu->description); ?></div>
                            </a>
                        </td>
                        <td>
                            <div class="sayi"><?php echo e(number_format(count($konu->mesajlar()))); ?></div>
                        </td>
                        <td>
                            <?php if($konu->sonMesaj()): ?>
                                Son Mesaj:
                                <a href="#" style="color: blue">
                                    <?php echo e($konu->sonMesaj()->user()->name); ?>

                                </a> <br>
                                <?php echo e($konu->sonMesaj()->timeAgo()); ?>

                            <?php else: ?>
                                Henüz cevap yazılmadı.
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </table>
            <?php echo $konular->links(); ?>


            <br><br>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\asame\Desktop\Projects\clients\ozfinansis\resources\views/forum/subject.blade.php ENDPATH**/ ?>