<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::get('backup/haberler', 'BackupController@haberler');

Route::get('/user-check', 'UserController@userCeck');

Route::prefix('{lang?}')->middleware('locale')->group(function () {
    Route::get('/', 'PagesController@home');

    Route::get('forum', 'PagesController@forumIndex');
    Route::get('forum/login', 'PagesController@forumLogin');
    Route::post('forum/login', 'PagesController@forumLoginPost');
    Route::get('forum/register', 'PagesController@forumRegister');
    Route::post('forum/register', 'PagesController@forumRegisterPost');
    Route::post('forum/register-step2', 'PagesController@forumRegisterPostStep2');
    Route::get('forum/{forum_id}', 'PagesController@forumSubject');
    Route::get('forum/{forum_id}/{konu_id}', 'PagesController@forumQuestion');
    Route::post('forum/{forum_id}/{konu_id}', 'ForumMesajController@store');

    Route::get('haberler', 'PagesController@haberler');
    Route::get('haberler/{id}-{slug}', 'PagesController@haber');

    Route::get('duyurular', 'PagesController@duyurular');
    Route::get('duyurular/{id}-{slug}', 'PagesController@duyuru');

    Route::get('iletisim', 'PagesController@iletisim');
    Route::post('iletisim', 'PagesController@iletisimPost');

    Route::get('fotograflar', 'PagesController@galeries');
    Route::get('fotograflar/{id}-{slug}', 'PagesController@galery');

    Route::get('videolar', 'PagesController@videos');
    Route::get('videolar/{id}-{slug}', 'PagesController@video');

    Route::get('sayfa/{id}-{slug}', 'PagesController@sayfa');

    Route::get('yayinlar', 'PagesController@yayinlar');

    Route::get('yonetim-kurulu', 'PagesController@yonetimKurulu');
    Route::get('yonetim-kurulu/{id}-{slug}', 'PagesController@yonetimKuruluKisi');

    Route::get('anlasmalar', 'PagesController@anlasmalar');
    Route::get('anlasmalar/{id}-{slug}', 'PagesController@anlasma');

    Route::get('basinda-biz', 'PagesController@basin');
    Route::get('basin-aciklamalari', 'PagesController@basinAciklamalari');

    Route::get('baskandan-mesajlar', 'PagesController@baskandanMesajlar');
    Route::get('baskandan-mesajlar/{id}', 'PagesController@baskandanMesaj');
});
