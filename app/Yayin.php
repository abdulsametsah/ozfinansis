<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Resizable;

class Yayin extends Model
{
    use Resizable;

    public function url()
    {
        $files = \json_decode($this->file);
        $file = $files[0];
        return asset("storage/" . $file->download_link);
    }
}
