<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Resizable;

class Haber extends Model
{
    use Resizable;

    public function url()
    {
        return url(app()->getLocale() . '/haberler/' . $this->id . '-' . $this->slug);
    }
}
