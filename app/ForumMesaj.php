<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\ForumKonu;

class ForumMesaj extends Model
{
    public function user()
    {
    	return User::find($this->user_id);
    }

    public function konu()
    {
    	return ForumKonu::find($this->konu_id);
    }

    public function timeAgo()
    {
    	$date = $this->created_at;
		$timestamp = strtotime($date);	

		$strTime = array("saniye", "dakika", "saat", "gün", "ay", "yıl");
		$length = array("60","60","24","30","12","10");

		$currentTime = time();
		if($currentTime >= $timestamp) {
			$diff     = time()- $timestamp;
			for($i = 0; $diff >= $length[$i] && $i < count($length)-1; $i++) {
			$diff = $diff / $length[$i];
			}

			$diff = round($diff);
			return $diff . " " . $strTime[$i] . " önce";
		}
    }
}
