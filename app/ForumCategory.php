<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ForumCategory extends Model
{
    public function url()
    {
        return url( app()->getLocale() . '/forum/' . $this->id . '-' . $this->slug);
    }
}
