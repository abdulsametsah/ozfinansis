<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Resizable;

class Anlasma extends Model
{
    use Resizable;

    public function url()
    {
        return url(app()->getLocale() . '/anlasmalar/' . $this->id . '-' . $this->slug);
    }
}
