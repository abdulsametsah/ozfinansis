<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Haber;

class BackupController extends Controller
{
    public $oldDb;
    public $newDb;
    
    public function __construct() {
        $this->oldDb = new \PDO("mysql:host=localhost;dbname=ozfinans_old;charset=utf8", "root", "");
    }

    public function haberler()
    {
        $oldHaberler = $this->oldDb->query("SELECT * FROM haber")->fetchAll();
        
        foreach ($oldHaberler as $haber) {
            $haber = (object) $haber;
            
            $h = new Haber;
            $h->title = $haber->haberbasligi;
            $h->summary = $haber->haberozeti;
            $h->body = $haber->habericerigi;
            $h->hit = $haber->goruntulenmesayisi;
        }
    }
}
