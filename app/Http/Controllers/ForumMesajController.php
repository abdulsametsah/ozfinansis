<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Forum;
use App\ForumKonu;
use App\ForumMesaj;


class ForumMesajController extends Controller
{
    public function store($lang, $forum_id, $konu_id, Request $req)
    {
    	if (!auth()->check()) {
    		return [
    			'status' => 'error',
    			'message' => "Lütfen giriş yap."
    		];
    	}

    	$content = $req->mesaj;

    	if (strlen($content) < 5) {
    		return [
    			'status' => 'error',
    			'message' => "Cevap en az 5 karakterden oluşmalı."
    		];
    	}

    	$mesaj = new ForumMesaj;
    	$mesaj->user_id = auth()->user()->id;
    	$mesaj->konu_id = $konu_id;
    	$mesaj->content = $content;
    	$mesaj->save();

    	return [
			'status' => 'success',
			'message' => "Cevap yazıldı."
		];
    }
}
