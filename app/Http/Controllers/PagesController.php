<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Haber;
use App\Duyuru;
use App\Galery;
use App\Video;
use App\Sayfa;
use App\Yayin;
use App\ForumCategory;
use App\Message;
use App\Kurul;
use App\Anlasma;
use App\Basin;
use App\BaskanMesaj;
use App\Forum;
use App\ForumKonu;
use App\ForumMesaj;

class PagesController extends Controller
{
    public function home()
    {
        $anlasmalar = Anlasma::latest()->get();
        $slider_haberler = Haber::where('basin_aciklamasi', false)->latest()->where('slider', true)->where('lang', app()->getLocale())->limit(10)->get();
        $alt_haberler = Haber::where('basin_aciklamasi', false)->latest()->where('slider', false)->where('lang', app()->getLocale())->limit(3)->get();

        return view("home", compact('alt_haberler', "anlasmalar", "slider_haberler"));
    }

    // Forum

    public function forumLogin()
    {
        return view("forum.login");
    }

    public function forumLoginPost(Request $request)
    {
        $email = $request->email;
        $password = $request->password;

        if (!$email || !$password) {
            return [
                'status' => "error",
                'message' => "Lütfen boş alan bırakmayınız."
            ];
        }

        if (auth()->attempt(['email' => $email, 'password' => $password])) {
            return [
                'status' => "success"
            ];
        }

        return [
            'status' => "error",
            'message' => "Bilgileriniz hatalı."
        ];
    }

    public function forumRegisterPostStep2(Request $request)
    {
        $name = $request->name;
        $email = $request->email;
        $password = $request->password;
        $tc = $request->tc;
        $telefon = $request->telefon;

        if (!$name || !$email || !$password || !$tc || !$telefon) {
            return [
                'status' => "error",
                'message' => "Lütfen boş alan bırakmayınız."
            ];
        }

        $userC = new UserController();
        $check = $userC->getTC($tc);

        if ($check->durum != 1) {
            return [
                'status' => 'error',
                'message' => 'Sendikamıza üyeliğiniz bulunmamaktadır.'
            ];
        }

        if ($check->cepno != $request->telefon) {
            return [
                'status' => 'error',
                'message' => 'Girdiğiniz bilgiler hatalı.',
            ];
        }

        if (User::where('email', $email)->first()) {
            return [
                'status' => 'error',
                'message' => 'Bu e-posta adresi zaten kullanılıyor.',
            ];
        }

        if (User::where('tc', $tc)->first()) {
            return [
                'status' => 'error',
                'message' => 'Bu TC kimlik numarası zaten kayıtlı.',
            ];
        }

        try {
            $user = new User;
            $user->name = $name;
            $user->email = $email;
            $user->tc = $tc;
            $user->telefon = $telefon;
            $user->password = bcrypt($password);
            $user->save();

            auth()->loginUsingId($user->id);

            return [
                'status' => 'success',
                'message' => 'Başarıyla kayıt oldunuz.',
            ];

        } catch (\Exception $e) {
            return [
                'status' => 'error',
                'message' => 'Bir hata oluştu. Lütfen daha sonra tekrar deneyiniz.',
            ];
        }
    }

    public function forumRegisterPost(Request $request)
    {
        $userController = new UserController();
        $check = $userController->getTC($request->tc);
        if ($check->durum != 1) {
            return [
                'status' => 'error',
                'message' => 'Sendikamıza üyeliğiniz bulunmamaktadır.'
            ];
        }

        if ($check->cepno != $request->telefon) {
            return [
                'status' => 'error',
                'message' => 'Girdiğiniz bilgiler hatalı.',
            ];
        }

        return [
            'status' => 'success',
        ];
    }

    public function forumRegister()
    {
        return view("forum.register");
    }

    public function forumIndex()
    {
        $forumlar = Forum::all();
        return view("forum.index", compact('forumlar'));
    }

    public function forumSubject($lang, $forum_id)
    {
        $forum = Forum::find($forum_id);
        $konular = ForumKonu::where('forum_id', $forum_id)->paginate(12);
        return view("forum.subject", compact('forum', 'konular'));
    }

    public function forumQuestion($lang, $forum_id, $konu_id)
    {
        $konu = ForumKonu::find($konu_id);
        $mesajlar = ForumMesaj::where('konu_id', $konu_id)->paginate(12);
        return view("forum.question", compact("konu", "mesajlar"));
    }

    // Haberler
    public function haberler($lang, Request $req)
    {
        $haberler = Haber::where('basin_aciklamasi', false)->latest()->where('lang', app()->getLocale());
        $q = $req->q ?? false;

        if ($req->q) {
            $haberler = $haberler->where('title', 'like', '%' . $req->q . '%')
                ->orWhere('body', 'like', '%' . $req->q . '%')
                ->orWhere('summary', 'like', '%' . $req->q . '%');
        }

        $haberler = $haberler->paginate(12);
        return view("haberler.haberler", compact('haberler', 'q'));
    }

    public function basinAciklamalari($lang, Request $req)
    {
        $haberler = Haber::where('basin_aciklamasi', true)->latest()->where('lang', app()->getLocale());
        $q = $req->q ?? false;

        $haberler = $haberler->paginate(12);
        return view("haberler.basin", compact('haberler', 'q'));
    }

    public function haber($lang, $id, $slug)
    {
        $haber = Haber::find($id);
        $haber->hit++;
        $haber->save();

        return view("haberler.haber", compact('haber'));
    }

    // Duyurular
    public function duyurular($lang, Request $req)
    {
        $type = $req->type ?? "flow";
        $duyurular = Duyuru::latest()->where('lang', app()->getLocale())->paginate(12);
        return view("duyurular", compact("duyurular", "type"));
    }

    public function duyuru($lang, $id, $slug)
    {
        $duyuru = Duyuru::find($id);
        return view("duyuru", compact('duyuru'));
    }

    // İletişim
    public function iletisim()
    {
        return view("iletisim");
    }

    public function iletisimPost($lang, Request $req)
    {
        // A sample PHP Script to POST data using cURL
        // Data in JSON format

        $data = array(
            'secret' => env('GOOGLE_RECAPTCHA_SECRET'),
            'response' => $req->{"g-recaptcha-response"},
        );

        $verify = json_decode(httpPost("https://www.google.com/recaptcha/api/siteverify", $data));

        if (!$verify->success) {
            $req->session()->flash('error', 'Robot olmadığınızı doğrulayamadık. Lütfen sayfayı yenileyip tekrar deneyiniz.');
            return back()->withInput();
        }

        if (!$req->name || !$req->text || !$req->email || !$req->phone) {
            $req->session()->flash('error', 'Lütfen boş alan bırakmayınız.');
            return back()->withInput();
        }


        try {
            $m = new Message;
            $m->from_name = $req->name;
            $m->from_phone = $req->phone;
            $m->from_email = $req->email;
            $m->text = $req->text;
            $m->save();

            $req->session()->flash('success', 'Mesajınız bize ulaştı. Teşekkür ederiz.');
            return back();

        } catch (\Exception $e) {
            if (!$verify->success) {
                $req->session()->flash('error', 'Bir hata oluştu lütfen daha sonra tekrar deneyiniz.');
                return back()->withInput();
            }
        }
    }

    // Galeriler
    public function galeries()
    {
        $galeriler = Galery::latest()->where('lang', app()->getLocale())->paginate(12);
        return view("galeriler.galeriler", compact('galeriler'));
    }

    public function galery($lang, $id, $slug)
    {
        $galery = Galery::find($id);
        return view("galeriler.galeri", compact('galery'));
    }

    // videolar
    public function videos()
    {
        $videos = Video::latest()->where('lang', app()->getLocale())->paginate(12);
        return view("videolar.videolar", compact('videos'));
    }

    public function video($lang, $id, $slug)
    {
        $video = Video::find($id);
        return view("videolar.video", compact('video'));
    }

    // Sayfa
    public function sayfa($lang, $id, $slug)
    {
        $sayfa = Sayfa::findOrFail($id);
        $sayfa->hit++;
        $sayfa->save();

        return view("sayfa", compact('sayfa'));
    }

    // Yayınlar
    public function yayinlar()
    {
        $yayinlar = Yayin::latest()->paginate(12);
        return view("yayinlar", compact('yayinlar'));
    }

    // Yönetim Kurulu
    public function yonetimKurulu()
    {
        $kisiler = Kurul::orderBy('sira', 'asc')->get();
        return view("yonetim-kurulu.index", compact('kisiler'));
    }

    public function yonetimKuruluKisi($lang, $id, $slug)
    {
        $kisi = Kurul::find($id);
        $kisiler = Kurul::orderBy('sira', 'asc')->where('id', "!=", $kisi->id)->get();
        return view("yonetim-kurulu.kisi", compact('kisiler', 'kisi'));
    }

    // Anlaşmalar
    public function anlasmalar()
    {
        $anlasmalar = Anlasma::latest()->paginate(12);

        return view("anlasmalar.index", compact("anlasmalar"));
    }

    public function anlasma($lang, $id, $slug)
    {
        $anlasma = Anlasma::findOrFail($id);
        return view("anlasmalar.anlasma", compact('anlasma'));
    }

    // Basında Biz
    public function basin()
    {
        $basindakiler = Basin::latest()->paginate(12);
        return view("basin", compact('basindakiler'));
    }

    // Başkandan Mesajlar
    public function baskandanMesajlar()
    {
        $mesajlar = BaskanMesaj::latest()->where('lang', app()->getLocale())->paginate(24);

        return view("baskan.mesajlar", compact('mesajlar'));
    }

    public function baskandanMesaj($lang, $id)
    {
        $mesaj = BaskanMesaj::findOrFail($id);

        return view("baskan.mesaj", compact('mesaj'));
    }
}
