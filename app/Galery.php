<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Resizable;

class Galery extends Model
{
    use Resizable;

    public function url()
    {
        return url(app()->getLocale() . '/fotograflar/' . $this->id . '-' . $this->slug);
    }
}
