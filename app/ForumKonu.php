<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Forum;
use App\ForumMesaj;

class ForumKonu extends Model
{
    public function forum()
    {
    	return Forum::find($this->forum_id);
    }

    public function mesajlar()
    {
    	return ForumMesaj::where('konu_id', $this->id)->get();
    }

    public function sonMesaj() {
        return ForumMesaj::where('konu_id', $this->id)->latest()->first();
    }
}
