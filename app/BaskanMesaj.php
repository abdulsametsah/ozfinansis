<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class BaskanMesaj extends Model
{
    public function url()
    {
        return url(app()->getLocale() . "/baskandan-mesajlar/" . $this->id);
    }
}
