<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ForumKonu;

class Forum extends Model
{
    public function konular()
    {
        return ForumKonu::where('forum_id', $this->id)->get();
    }

    public function url()
    {
        return url(app()->getLocale() . "/forum/" . $this->id);
    }

    public function mesajSayisi()
    {
        $toplam = 0;
        foreach ($this->konular() as $konu) {
            $toplam += count($konu->mesajlar());
        }

        return $toplam;
    }

    public function sonMesaj()
    {
        $sonMesajlar = [];
        foreach ($this->konular() as $konu) {
            if ($konu->sonMesaj()) {
                $sonMesajlar[strtotime($konu->sonMesaj()->created_at)] = $konu->sonMesaj();
            }
        }

        if (count($sonMesajlar) == 0) return false;
        sort($sonMesajlar);

        return $sonMesajlar[0];
    }
}
