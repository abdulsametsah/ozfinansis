<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Sayfa extends Model
{
    public function url()
    {
        return url( app()->getLocale() . '/sayfa/' . $this->id . '-' . $this->slug);
    }
}
