<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Resizable;


class Kurul extends Model
{
    use Resizable;

    public function url()
    {
        return url(app()->getLocale() . '/yonetim-kurulu/' . $this->id . '-' . $this->slug);
    }
}
