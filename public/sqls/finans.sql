-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 08, 2019 at 11:59 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `finans`
--

-- --------------------------------------------------------

--
-- Table structure for table `anlasmas`
--

CREATE TABLE `anlasmas` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `summary` text COLLATE utf8mb4_unicode_ci,
  `body` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `anlasmas`
--

INSERT INTO `anlasmas` (`id`, `name`, `image`, `summary`, `body`, `created_at`, `updated_at`, `slug`) VALUES
(1, 'Deneme anlaşma', 'anlasmas/July2019/X8miSRuH3FvqEWyZhTML.jpg', 'Deneme özet bilgi', '<p>deneme a&ccedil;ıklama</p>', '2019-07-03 20:00:00', '2019-07-04 14:05:14', 'deneme-anlasma');

-- --------------------------------------------------------

--
-- Table structure for table `basins`
--

CREATE TABLE `basins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `basins`
--

INSERT INTO `basins` (`id`, `name`, `photo`, `created_at`, `updated_at`) VALUES
(1, 'AbdulSamet ŞAHİN', 'basins/July2019/3ODToMpE2e5TbN7pud3L.jpg', '2019-07-04 14:54:12', '2019-07-04 14:54:12');

-- --------------------------------------------------------

--
-- Table structure for table `baskan_mesajs`
--

CREATE TABLE `baskan_mesajs` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `lang` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'tr'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `baskan_mesajs`
--

INSERT INTO `baskan_mesajs` (`id`, `title`, `body`, `created_at`, `updated_at`, `lang`) VALUES
(1, 'Deneme mesaj', '<p>djashndlaskd</p>', '2019-07-06 23:11:10', '2019-07-06 23:11:10', 'tr'),
(2, 'Deneme mesaj 2', '<p>kajsndlsad</p>', '2019-07-06 23:11:19', '2019-07-06 23:11:19', 'tr');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `order`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, NULL, 1, 'Category 1', 'category-1', '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(2, NULL, 1, 'Category 2', 'category-2', '2019-06-29 13:18:37', '2019-06-29 13:18:37');

-- --------------------------------------------------------

--
-- Table structure for table `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, NULL, 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, NULL, 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, NULL, 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, NULL, 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, NULL, 12),
(12, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(13, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(14, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(15, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(17, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(18, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(19, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(20, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
(21, 1, 'role_id', 'text', 'Role', 1, 1, 1, 1, 1, 1, NULL, 9),
(22, 4, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(23, 4, 'parent_id', 'select_dropdown', 'Parent', 0, 0, 1, 1, 1, 1, '{\"default\":\"\",\"null\":\"\",\"options\":{\"\":\"-- None --\"},\"relationship\":{\"key\":\"id\",\"label\":\"name\"}}', 2),
(24, 4, 'order', 'text', 'Order', 1, 1, 1, 1, 1, 1, '{\"default\":1}', 3),
(25, 4, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 4),
(26, 4, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"name\"}}', 5),
(27, 4, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 0, NULL, 6),
(28, 4, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(29, 5, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(30, 5, 'author_id', 'text', 'Author', 1, 0, 1, 1, 0, 1, NULL, 2),
(31, 5, 'category_id', 'text', 'Category', 1, 0, 1, 1, 1, 0, NULL, 3),
(32, 5, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, NULL, 4),
(33, 5, 'excerpt', 'text_area', 'Excerpt', 1, 0, 1, 1, 1, 1, NULL, 5),
(34, 5, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, NULL, 6),
(35, 5, 'image', 'image', 'Post Image', 0, 1, 1, 1, 1, 1, '{\"resize\":{\"width\":\"1000\",\"height\":\"null\"},\"quality\":\"70%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"250\"}}]}', 7),
(36, 5, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true},\"validation\":{\"rule\":\"unique:posts,slug\"}}', 8),
(37, 5, 'meta_description', 'text_area', 'Meta Description', 1, 0, 1, 1, 1, 1, NULL, 9),
(38, 5, 'meta_keywords', 'text_area', 'Meta Keywords', 1, 0, 1, 1, 1, 1, NULL, 10),
(39, 5, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"DRAFT\",\"options\":{\"PUBLISHED\":\"published\",\"DRAFT\":\"draft\",\"PENDING\":\"pending\"}}', 11),
(40, 5, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 12),
(41, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 13),
(42, 5, 'seo_title', 'text', 'SEO Title', 0, 1, 1, 1, 1, 1, NULL, 14),
(43, 5, 'featured', 'checkbox', 'Featured', 1, 1, 1, 1, 1, 1, NULL, 15),
(44, 6, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(45, 6, 'author_id', 'text', 'Author', 1, 0, 0, 0, 0, 0, NULL, 2),
(46, 6, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, NULL, 3),
(47, 6, 'excerpt', 'text_area', 'Excerpt', 1, 0, 1, 1, 1, 1, NULL, 4),
(48, 6, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, NULL, 5),
(49, 6, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\"},\"validation\":{\"rule\":\"unique:pages,slug\"}}', 6),
(50, 6, 'meta_description', 'text', 'Meta Description', 1, 0, 1, 1, 1, 1, NULL, 7),
(51, 6, 'meta_keywords', 'text', 'Meta Keywords', 1, 0, 1, 1, 1, 1, NULL, 8),
(52, 6, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"INACTIVE\",\"options\":{\"INACTIVE\":\"INACTIVE\",\"ACTIVE\":\"ACTIVE\"}}', 9),
(53, 6, 'created_at', 'timestamp', 'Created At', 1, 1, 1, 0, 0, 0, NULL, 10),
(54, 6, 'updated_at', 'timestamp', 'Updated At', 1, 0, 0, 0, 0, 0, NULL, 11),
(55, 6, 'image', 'image', 'Page Image', 0, 1, 1, 1, 1, 1, NULL, 12),
(56, 7, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(57, 7, 'title', 'text', 'Başlık', 0, 1, 1, 1, 1, 1, '{}', 4),
(58, 7, 'summary', 'text_area', 'Özet', 0, 0, 1, 1, 1, 1, '{}', 6),
(59, 7, 'body', 'rich_text_box', 'İçerik', 0, 0, 1, 1, 1, 1, '{}', 7),
(60, 7, 'hit', 'number', 'Görüntülenme', 0, 1, 1, 0, 0, 0, '{}', 8),
(61, 7, 'comments', 'number', 'Yorum Sayısı', 0, 1, 1, 0, 0, 0, '{}', 9),
(62, 7, 'video', 'file', 'Video', 0, 0, 1, 1, 1, 1, '{}', 10),
(63, 7, 'photos', 'multiple_images', 'İlgili Fotoğraflar', 0, 0, 1, 1, 1, 1, '{\"thumbnails\":[{\"name\":\"127x63\",\"crop\":{\"width\":\"127\",\"height\":\"63\"}},{\"name\":\"494x246\",\"crop\":{\"width\":\"494\",\"height\":\"246\"}},{\"name\":\"kare\",\"crop\":{\"width\":\"600\",\"height\":\"600\"}}]}', 11),
(64, 7, 'slug', 'text', 'URL (SLUG)', 0, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true}}', 5),
(65, 7, 'image', 'image', 'Kapak Fotoğrafı', 0, 0, 1, 1, 1, 1, '{\"thumbnails\":[{\"name\":\"slider\",\"crop\":{\"width\":\"815\",\"height\":\"416\"}},{\"name\":\"127x63\",\"crop\":{\"width\":\"127\",\"height\":\"63\"}},{\"name\":\"500x300\",\"crop\":{\"width\":\"500\",\"height\":\"300\"}},{\"name\":\"kare\",\"crop\":{\"width\":\"600\",\"height\":\"600\"}}]}', 3),
(66, 7, 'created_at', 'timestamp', 'Eklenme Tarihi', 0, 1, 1, 0, 0, 0, '{}', 12),
(67, 7, 'updated_at', 'timestamp', 'Güncelleme Tarihi', 0, 0, 0, 0, 0, 0, '{}', 13),
(68, 7, 'active', 'checkbox', 'Yayınla', 0, 1, 1, 1, 1, 1, '{}', 14),
(69, 8, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(70, 8, 'title', 'text', 'Başlık', 0, 1, 1, 1, 1, 1, '{}', 2),
(71, 8, 'slug', 'text', 'URL (SLUG)', 0, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true}}', 3),
(72, 8, 'summary', 'text_area', 'Özet', 0, 0, 1, 1, 1, 1, '{}', 4),
(73, 8, 'body', 'rich_text_box', 'İçerik', 0, 0, 1, 1, 1, 1, '{}', 5),
(74, 8, 'image', 'image', 'Fotoğraf', 0, 0, 1, 1, 1, 1, '{\"thumbnails\":[{\"name\":\"127x63\",\"crop\":{\"width\":\"127\",\"height\":\"63\"}},{\"name\":\"494x246\",\"crop\":{\"width\":\"494\",\"height\":\"246\"}},{\"name\":\"kare\",\"crop\":{\"width\":\"600\",\"height\":\"600\"},\"upsize\":true}]}', 6),
(75, 8, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 7),
(76, 8, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 8),
(77, 9, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(78, 9, 'name', 'text', 'İsim', 0, 1, 1, 1, 1, 1, '{}', 2),
(79, 9, 'image', 'image', 'Fotoğraf', 0, 0, 1, 1, 1, 1, '{\"thumbnails\":[{\"name\":\"127x63\",\"crop\":{\"width\":\"127\",\"height\":\"63\"}},{\"name\":\"494x246\",\"crop\":{\"width\":\"494\",\"height\":\"246\"}},{\"name\":\"kare\",\"crop\":{\"width\":\"600\",\"height\":\"600\"},\"upsize\":true}]}', 3),
(80, 9, 'file', 'file', 'Dosya', 0, 0, 1, 1, 1, 1, '{}', 4),
(81, 9, 'url', 'text', 'Url (Varsa)', 0, 0, 1, 1, 1, 1, '{}', 5),
(82, 9, 'created_at', 'timestamp', 'Yüklenme Tarihi', 0, 1, 1, 1, 0, 1, '{}', 6),
(83, 9, 'updated_at', 'timestamp', 'Güncelleme Tarihi', 0, 0, 0, 0, 0, 0, '{}', 7),
(84, 10, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(85, 10, 'name', 'text', 'Galeri Adı', 0, 1, 1, 1, 1, 1, '{}', 2),
(86, 10, 'cover', 'image', 'Kapak Fotoğrafı', 0, 0, 1, 1, 1, 1, '{\"thumbnails\":[{\"name\":\"127x63\",\"crop\":{\"width\":\"127\",\"height\":\"63\"}},{\"name\":\"494x246\",\"crop\":{\"width\":\"494\",\"height\":\"246\"}},{\"name\":\"kare\",\"crop\":{\"width\":\"600\",\"height\":\"600\"}}]}', 3),
(87, 10, 'photos', 'multiple_images', 'Fotoğraflar', 0, 0, 1, 1, 1, 1, '{\"thumbnails\":[{\"name\":\"127x63\",\"crop\":{\"width\":\"127\",\"height\":\"63\"}},{\"name\":\"494x246\",\"crop\":{\"width\":\"494\",\"height\":\"246\"}},{\"name\":\"kare\",\"crop\":{\"width\":\"600\",\"height\":\"600\"}}]}', 4),
(88, 10, 'created_at', 'timestamp', 'Yüklenme Tarihi', 0, 1, 1, 1, 0, 1, '{}', 5),
(89, 10, 'updated_at', 'timestamp', 'Güncelleme Tarihi', 0, 0, 0, 0, 0, 0, '{}', 6),
(90, 10, 'slug', 'text', 'Slug', 0, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"name\"}}', 7),
(91, 11, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(92, 11, 'title', 'text', 'Başlık', 0, 1, 1, 1, 1, 1, '{}', 2),
(93, 11, 'slug', 'text', 'URL (SLUG)', 0, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true}}', 3),
(94, 11, 'image', 'image', 'Kapak Fotoğrafı', 0, 0, 1, 1, 1, 1, '{\"thumbnails\":[{\"name\":\"127x63\",\"crop\":{\"width\":\"127\",\"height\":\"63\"}},{\"name\":\"494x246\",\"crop\":{\"width\":\"494\",\"height\":\"246\"}},{\"name\":\"kare\",\"crop\":{\"width\":\"600\",\"height\":\"600\"}}]}', 4),
(95, 11, 'videos', 'file', 'Video', 0, 0, 1, 1, 1, 1, '{}', 5),
(96, 11, 'created_at', 'timestamp', 'Tarih', 0, 1, 1, 1, 0, 1, '{}', 6),
(97, 11, 'updated_at', 'timestamp', 'Güncelleme Tarihi', 0, 0, 0, 0, 0, 0, '{}', 7),
(98, 12, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(99, 12, 'title', 'text', 'Başlık', 0, 1, 1, 1, 1, 1, '{}', 2),
(100, 12, 'body', 'rich_text_box', 'İçerik', 0, 0, 1, 1, 1, 1, '{}', 3),
(101, 12, 'hit', 'number', 'Görüntülenme', 0, 0, 1, 0, 0, 1, '{}', 4),
(102, 12, 'created_at', 'timestamp', 'Tarih', 0, 1, 1, 1, 0, 1, '{}', 5),
(103, 12, 'updated_at', 'timestamp', 'Güncelleme Tarihi', 0, 0, 0, 0, 0, 0, '{}', 6),
(104, 12, 'slug', 'text', 'Slug', 0, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\"}}', 7),
(105, 14, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(106, 14, 'name', 'text', 'İsim', 0, 1, 1, 1, 1, 1, '{}', 2),
(107, 14, 'slug', 'text', 'Slug', 0, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"name\"}}', 3),
(108, 14, 'description', 'text', 'Açıklama', 0, 1, 1, 1, 1, 1, '{}', 4),
(109, 14, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 5),
(110, 14, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(111, 15, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(112, 15, 'from_name', 'text', 'Göderen İsim', 0, 1, 1, 1, 1, 1, '{}', 2),
(113, 15, 'from_email', 'text', 'E-Posta', 0, 1, 1, 1, 1, 1, '{}', 3),
(114, 15, 'from_phone', 'text', 'Telefon', 0, 0, 1, 1, 1, 1, '{}', 4),
(115, 15, 'text', 'text', 'Mesaj', 0, 0, 1, 1, 1, 1, '{}', 5),
(116, 15, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 6),
(117, 15, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(118, 16, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(119, 16, 'name', 'text', 'İsim', 0, 1, 1, 1, 1, 1, '{}', 2),
(120, 16, 'image', 'image', 'Fotoğraf', 0, 0, 1, 1, 1, 1, '{\"thumbnails\":[{\"name\":\"127x63\",\"crop\":{\"width\":\"127\",\"height\":\"63\"}},{\"name\":\"500x300\",\"crop\":{\"width\":\"500\",\"height\":\"300\"}},{\"name\":\"kare\",\"crop\":{\"width\":\"600\",\"height\":\"600\"}}]}', 3),
(121, 16, 'summary', 'text_area', 'Özet Bilgi', 0, 0, 1, 1, 1, 1, '{}', 4),
(122, 16, 'body', 'rich_text_box', 'Açıklama', 0, 0, 1, 1, 1, 1, '{}', 5),
(123, 16, 'created_at', 'timestamp', 'Tarih', 0, 0, 1, 1, 0, 1, '{}', 6),
(124, 16, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(125, 16, 'slug', 'text', 'Slug', 0, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"name\"}}', 8),
(126, 17, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(127, 17, 'name', 'text', 'İsim', 0, 1, 1, 1, 1, 1, '{}', 2),
(128, 17, 'slug', 'text', 'Slug', 0, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"name\"}}', 3),
(129, 17, 'kurum', 'text', 'Kurum', 0, 1, 1, 1, 1, 1, '{}', 5),
(130, 17, 'gorev', 'text', 'Görev', 0, 1, 1, 1, 1, 1, '{}', 6),
(131, 17, 'bio', 'rich_text_box', 'Hakkında', 0, 1, 1, 1, 1, 1, '{}', 7),
(132, 17, 'facebook', 'text', 'Facebook', 0, 1, 1, 1, 1, 1, '{}', 8),
(133, 17, 'twitter', 'text', 'Twitter', 0, 1, 1, 1, 1, 1, '{}', 9),
(134, 17, 'linkedin', 'text', 'Linkedin', 0, 1, 1, 1, 1, 1, '{}', 10),
(135, 17, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 11),
(136, 17, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 12),
(137, 17, 'sira', 'number', 'Gösterim Sırası', 0, 1, 1, 1, 1, 1, '{}', 13),
(138, 17, 'image', 'image', 'Fotoğraf', 0, 1, 1, 1, 1, 1, '{\"thumbnails\":[{\"name\":\"127x63\",\"crop\":{\"width\":\"127\",\"height\":\"63\"}},{\"name\":\"494x246\",\"crop\":{\"width\":\"494\",\"height\":\"246\"}},{\"name\":\"kare\",\"crop\":{\"width\":\"600\",\"height\":\"600\"},\"upsize\":true}]}', 4),
(139, 18, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(140, 18, 'name', 'text', 'Başlık', 0, 1, 1, 1, 1, 1, '{}', 2),
(141, 18, 'photo', 'image', 'Fotoğraf', 0, 1, 1, 1, 1, 1, '{\"thumbnails\":[{\"name\":\"127x63\",\"crop\":{\"width\":\"127\",\"height\":\"63\"}},{\"name\":\"494x246\",\"crop\":{\"width\":\"494\",\"height\":\"246\"}},{\"name\":\"kare\",\"crop\":{\"width\":\"600\",\"height\":\"600\"}}]}', 3),
(142, 18, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 4),
(143, 18, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(144, 7, 'lang', 'select_dropdown', 'Dil', 0, 1, 1, 1, 1, 1, '{\"default\":\"tr\",\"options\":{\"tr\":\"T\\u00fcrk\\u00e7e\",\"en\":\"\\u0130ngilizce\"}}', 2),
(145, 7, 'slider', 'checkbox', 'Sliderda gözüksün?', 0, 1, 1, 1, 1, 1, '{}', 15),
(146, 7, 'basin_aciklamasi', 'checkbox', 'Basin Aciklamasi', 0, 1, 1, 1, 1, 1, '{}', 16),
(147, 19, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(148, 19, 'title', 'text', 'Başlık', 0, 1, 1, 1, 1, 1, '{}', 2),
(149, 19, 'body', 'rich_text_box', 'İçerik', 0, 1, 1, 1, 1, 1, '{}', 3),
(150, 19, 'created_at', 'timestamp', 'Tarih', 0, 1, 1, 1, 0, 1, '{}', 4),
(151, 19, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(152, 19, 'lang', 'select_dropdown', 'Dil', 0, 1, 1, 1, 1, 1, '{\"default\":\"tr\",\"options\":{\"tr\":\"T\\u00fcrk\\u00e7e\",\"en\":\"\\u0130ngilizce\"}}', 6),
(153, 8, 'lang', 'select_dropdown', 'Dil', 0, 1, 1, 1, 1, 1, '{\"default\":\"tr\",\"options\":{\"tr\":\"T\\u00fcrk\\u00e7e\",\"en\":\"\\u0130ngilizce\"}}', 9),
(154, 11, 'lang', 'select_dropdown', 'Dil', 0, 1, 1, 1, 1, 1, '{\"default\":\"tr\",\"options\":{\"tr\":\"T\\u00fcrk\\u00e7e\",\"en\":\"\\u0130ngilizce\"}}', 8),
(155, 10, 'lang', 'select_dropdown', 'Dil', 0, 1, 1, 1, 1, 1, '{\"default\":\"tr\",\"options\":{\"tr\":\"T\\u00fcrk\\u00e7e\",\"en\":\"\\u0130ngilizce\"}}', 8);

-- --------------------------------------------------------

--
-- Table structure for table `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController', '', 1, 0, NULL, '2019-06-29 13:18:36', '2019-06-29 13:18:36'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2019-06-29 13:18:36', '2019-06-29 13:18:36'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, '', '', 1, 0, NULL, '2019-06-29 13:18:36', '2019-06-29 13:18:36'),
(4, 'categories', 'categories', 'Category', 'Categories', 'voyager-categories', 'TCG\\Voyager\\Models\\Category', NULL, '', '', 1, 0, NULL, '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(5, 'posts', 'posts', 'Post', 'Posts', 'voyager-news', 'TCG\\Voyager\\Models\\Post', 'TCG\\Voyager\\Policies\\PostPolicy', '', '', 1, 0, NULL, '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(6, 'pages', 'pages', 'Page', 'Pages', 'voyager-file-text', 'TCG\\Voyager\\Models\\Page', NULL, '', '', 1, 0, NULL, '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(7, 'habers', 'habers', 'Haber', 'Haberler', NULL, 'App\\Haber', NULL, NULL, NULL, 1, 1, '{\"order_column\":\"created_at\",\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null,\"scope\":null}', '2019-07-01 20:24:37', '2019-07-06 21:13:49'),
(8, 'duyurus', 'duyurus', 'Duyuru', 'Duyurular', NULL, 'App\\Duyuru', NULL, NULL, NULL, 1, 1, '{\"order_column\":\"created_at\",\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":\"title\",\"scope\":null}', '2019-07-02 05:23:05', '2019-07-06 23:27:15'),
(9, 'yayins', 'yayins', 'Yayın', 'Yayınlar', NULL, 'App\\Yayin', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-07-02 06:01:57', '2019-07-02 06:02:29'),
(10, 'galeries', 'galeries', 'Galeri', 'Galeriler', NULL, 'App\\Galery', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-07-02 08:24:09', '2019-07-06 23:32:27'),
(11, 'videos', 'videos', 'Video', 'Videolar', 'voyager-video', 'App\\Video', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-07-02 12:21:58', '2019-07-06 23:31:31'),
(12, 'sayfas', 'sayfas', 'Sayfa', 'Sayfalar', NULL, 'App\\Sayfa', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-07-03 10:55:40', '2019-07-03 11:02:53'),
(14, 'forum_categories', 'forum-categories', 'Forum Kategori', 'Forum Kategorileri', NULL, 'App\\ForumCategory', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2019-07-03 12:13:11', '2019-07-03 12:13:11'),
(15, 'messages', 'messages', 'Mesaj', 'Mesajlar', NULL, 'App\\Message', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2019-07-03 12:19:43', '2019-07-03 12:19:43'),
(16, 'anlasmas', 'anlasmas', 'Anlasma', 'Anlasmalar', NULL, 'App\\Anlasma', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-07-03 20:00:23', '2019-07-04 14:05:04'),
(17, 'kuruls', 'kuruls', 'Kurul Üyesi', 'Kurul Üyeleri', NULL, 'App\\Kurul', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-07-04 13:18:36', '2019-07-04 13:19:27'),
(18, 'basins', 'basins', 'Basın', 'Basındakiler', NULL, 'App\\Basin', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-07-04 14:46:37', '2019-07-04 14:53:53'),
(19, 'baskan_mesajs', 'baskan-mesajs', 'Başkandan Mesaj', 'Başkandan Mesajlar', NULL, 'App\\BaskanMesaj', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-07-06 23:10:28', '2019-07-06 23:26:58');

-- --------------------------------------------------------

--
-- Table structure for table `duyurus`
--

CREATE TABLE `duyurus` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `summary` text COLLATE utf8mb4_unicode_ci,
  `body` longtext COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `lang` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'tr'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `duyurus`
--

INSERT INTO `duyurus` (`id`, `title`, `slug`, `summary`, `body`, `image`, `created_at`, `updated_at`, `lang`) VALUES
(1, 'Deneme duyuru', 'deneme-duyuru', 'özet', '<p>djasdnkasldad</p>', 'duyurus\\July2019\\mZoGLOQO47HAoH7UBVGo.jpg', '2019-07-02 05:24:00', '2019-07-02 05:27:06', 'tr'),
(2, 'Deneme duyuru 2', 'deneme-duyuru-2', 'deneme', '<p>dasdasd</p>', 'duyurus\\July2019\\9UBVZQwpFLREmm83t7m6.jpg', '2019-07-02 05:25:00', '2019-07-02 05:26:54', 'tr');

-- --------------------------------------------------------

--
-- Table structure for table `forum_categories`
--

CREATE TABLE `forum_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `forum_categories`
--

INSERT INTO `forum_categories` (`id`, `name`, `slug`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Deneme kategori', 'deneme-kategori', 'deneme açıklama', '2019-07-03 12:13:37', '2019-07-03 12:13:37');

-- --------------------------------------------------------

--
-- Table structure for table `galeries`
--

CREATE TABLE `galeries` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cover` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photos` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lang` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'tr'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `galeries`
--

INSERT INTO `galeries` (`id`, `name`, `cover`, `photos`, `created_at`, `updated_at`, `slug`, `lang`) VALUES
(1, 'Deneme galeri2', 'galeries\\July2019\\FK4lHUjYtCaM4WNM93Qc.jpg', '[\"galeries\\\\July2019\\\\n7IXSEGB53S7b0DCD3pp.jpg\",\"galeries\\\\July2019\\\\lb2I4ARbOns9ciErhLQG.jpg\",\"galeries\\\\July2019\\\\7aB4WByr9VYMcNF5h9XE.jpg\",\"galeries\\\\July2019\\\\7zyA7vX7kbnwkRAzXIJ1.jpg\"]', '2019-07-02 08:29:00', '2019-07-02 08:34:11', 'deneme-galeri2', 'tr'),
(2, 'Deneme Galeri 22', 'galeries\\July2019\\1P6wMUOwFocuw9G6PkK2.jpg', '[\"galeries\\\\July2019\\\\RhHXH1Iy0UsFFvbKSPf1.jpg\",\"galeries\\\\July2019\\\\JsLG8a3yRVBpBKyTr6pt.jpg\"]', '2019-07-02 08:30:00', '2019-07-02 08:34:05', 'deneme-galeri-22', 'tr');

-- --------------------------------------------------------

--
-- Table structure for table `habers`
--

CREATE TABLE `habers` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `summary` text COLLATE utf8mb4_unicode_ci,
  `body` longtext COLLATE utf8mb4_unicode_ci,
  `hit` int(11) DEFAULT NULL,
  `comments` int(11) DEFAULT NULL,
  `video` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photos` longtext COLLATE utf8mb4_unicode_ci,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `active` int(11) DEFAULT '1',
  `lang` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'tr',
  `slider` int(11) DEFAULT '0',
  `basin_aciklamasi` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `habers`
--

INSERT INTO `habers` (`id`, `title`, `summary`, `body`, `hit`, `comments`, `video`, `photos`, `slug`, `image`, `created_at`, `updated_at`, `active`, `lang`, `slider`, `basin_aciklamasi`) VALUES
(8, 'Deneme haber', 'dasdsad', '<p>dasd</p>', 68, NULL, '[{\"download_link\":\"habers\\\\July2019\\\\vbJxGDOIM2cLiF44NJUG.mp4\",\"original_name\":\"mov_bbb.mp4\"}]', '[\"habers\\\\July2019\\\\91EuLmbus3dS3SVcE3fw.jpg\",\"habers\\\\July2019\\\\Z4U5ASbB1RQGPsbFhlNa.jpg\"]', 'deneme-haber', 'habers\\July2019\\mMRpmD4nujsizbIYrR2v.jpg', '2019-07-06 20:42:04', '2019-07-06 21:11:28', 1, 'tr', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kuruls`
--

CREATE TABLE `kuruls` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kurum` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gorev` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bio` longtext COLLATE utf8mb4_unicode_ci,
  `facebook` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkedin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `sira` int(11) DEFAULT '0',
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kuruls`
--

INSERT INTO `kuruls` (`id`, `name`, `slug`, `kurum`, `gorev`, `bio`, `facebook`, `twitter`, `linkedin`, `created_at`, `updated_at`, `sira`, `image`) VALUES
(1, 'AbdulSamet ŞAHİN', 'abdulsamet-sahin', 'Halk Bankası', 'Başkan', '<p>Deneme <strong>hakkında</strong></p>', NULL, NULL, NULL, '2019-07-04 13:20:14', '2019-07-04 13:20:14', 0, 'kuruls/July2019/pm7yecjtJ7mnkapEWz5Y.jpg'),
(2, 'Deneme Üye', 'deneme-uye', 'dasdasd', 'asdasd', '<p>asdasdasdasd</p>', NULL, NULL, NULL, '2019-07-04 13:28:04', '2019-07-04 13:28:04', 2, 'kuruls/July2019/0sxLJDZGfKW4w2tIcdut.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2019-06-29 13:18:36', '2019-06-29 13:18:36'),
(2, 'Site', '2019-06-29 16:27:07', '2019-06-29 16:27:07');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Anasayfa', '', '_self', 'voyager-boat', '#000000', NULL, 1, '2019-06-29 13:18:36', '2019-07-06 23:33:42', 'voyager.dashboard', 'null'),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 15, '2019-06-29 13:18:37', '2019-07-06 23:33:21', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 1, '2019-06-29 13:18:37', '2019-07-06 20:46:21', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 2, '2019-06-29 13:18:37', '2019-07-06 20:46:21', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 3, '2019-06-29 13:18:37', '2019-07-06 20:46:21', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 4, '2019-06-29 13:18:37', '2019-07-06 20:46:21', 'voyager.bread.index', NULL),
(10, 1, 'Ayarlar', '', '_self', 'voyager-settings', '#000000', NULL, 14, '2019-06-29 13:18:37', '2019-07-06 23:33:30', 'voyager.settings.index', 'null'),
(14, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 5, '2019-06-29 13:18:38', '2019-07-06 20:46:21', 'voyager.hooks', NULL),
(15, 2, 'Anasayfa', '/', '_self', NULL, '#000000', NULL, 15, '2019-06-29 16:27:36', '2019-06-29 16:27:36', NULL, ''),
(16, 1, 'Haberler', '', '_self', 'voyager-news', '#000000', NULL, 2, '2019-07-01 20:24:37', '2019-07-06 23:34:01', 'voyager.habers.index', 'null'),
(17, 1, 'Duyurular', '', '_self', 'voyager-paper-plane', '#000000', NULL, 3, '2019-07-02 05:23:05', '2019-07-06 23:34:34', 'voyager.duyurus.index', 'null'),
(18, 1, 'Yayınlar', '', '_self', 'voyager-book', '#000000', NULL, 4, '2019-07-02 06:01:57', '2019-07-06 23:34:59', 'voyager.yayins.index', 'null'),
(19, 1, 'Galeriler', '', '_self', 'voyager-photos', '#000000', NULL, 5, '2019-07-02 08:24:09', '2019-07-06 23:35:12', 'voyager.galeries.index', 'null'),
(20, 1, 'Videos', '', '_self', 'voyager-video', '#000000', NULL, 6, '2019-07-02 12:21:58', '2019-07-06 23:35:30', 'voyager.videos.index', 'null'),
(21, 1, 'Sayfalar', '', '_self', 'voyager-browser', '#000000', NULL, 7, '2019-07-03 10:55:40', '2019-07-06 23:35:50', 'voyager.sayfas.index', 'null'),
(23, 1, 'Mesajlar', '', '_self', 'voyager-mail', '#000000', NULL, 9, '2019-07-03 12:19:43', '2019-07-06 23:36:28', 'voyager.messages.index', 'null'),
(24, 1, 'Anlasmalar', '', '_self', 'voyager-diamond', '#000000', NULL, 10, '2019-07-03 20:00:23', '2019-07-06 23:36:43', 'voyager.anlasmas.index', 'null'),
(25, 1, 'Kurul Üyeleri', '', '_self', 'voyager-people', '#000000', NULL, 11, '2019-07-04 13:18:36', '2019-07-06 23:37:01', 'voyager.kuruls.index', 'null'),
(26, 1, 'Basinda Biz', '', '_self', 'voyager-file-text', '#000000', NULL, 12, '2019-07-04 14:46:37', '2019-07-06 23:37:56', 'voyager.basins.index', 'null'),
(27, 1, 'Başkandan Mesajlar', '', '_self', 'voyager-person', '#000000', NULL, 13, '2019-07-06 23:10:28', '2019-07-06 23:38:11', 'voyager.baskan-mesajs.index', 'null');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `from_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `from_name`, `from_email`, `from_phone`, `text`, `created_at`, `updated_at`) VALUES
(2, 'dasd', 'asd', 'asd', 'asdasd', '2019-07-03 12:47:15', '2019-07-03 12:47:15'),
(3, 'dasd', 'asd', 'asd', 'asdasd', '2019-07-03 12:47:30', '2019-07-03 12:47:30');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_05_19_173453_create_menu_table', 1),
(6, '2016_10_21_190000_create_roles_table', 1),
(7, '2016_10_21_190000_create_settings_table', 1),
(8, '2016_11_30_135954_create_permission_table', 1),
(9, '2016_11_30_141208_create_permission_role_table', 1),
(10, '2016_12_26_201236_data_types__add__server_side', 1),
(11, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(12, '2017_01_14_005015_create_translations_table', 1),
(13, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(14, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(15, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(16, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(17, '2017_08_05_000000_add_group_to_settings_table', 1),
(18, '2017_11_26_013050_add_user_role_relationship', 1),
(19, '2017_11_26_015000_create_user_roles_table', 1),
(20, '2018_03_11_000000_add_user_settings', 1),
(21, '2018_03_14_000000_add_details_to_data_types_table', 1),
(22, '2018_03_16_000000_make_settings_value_nullable', 1),
(23, '2016_01_01_000000_create_pages_table', 2),
(24, '2016_01_01_000000_create_posts_table', 2),
(25, '2016_02_15_204651_create_categories_table', 2),
(26, '2017_04_11_000000_alter_post_nullable_fields_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `author_id`, `title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 'Hello World', 'Hang the jib grog grog blossom grapple dance the hempen jig gangway pressgang bilge rat to go on account lugger. Nelsons folly gabion line draught scallywag fire ship gaff fluke fathom case shot. Sea Legs bilge rat sloop matey gabion long clothes run a shot across the bow Gold Road cog league.', '<p>Hello World. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', 'pages/page1.jpg', 'hello-world', 'Yar Meta Description', 'Keyword1, Keyword2', 'ACTIVE', '2019-06-29 13:18:37', '2019-06-29 13:18:37');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(2, 'browse_bread', NULL, '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(3, 'browse_database', NULL, '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(4, 'browse_media', NULL, '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(5, 'browse_compass', NULL, '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(6, 'browse_menus', 'menus', '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(7, 'read_menus', 'menus', '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(8, 'edit_menus', 'menus', '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(9, 'add_menus', 'menus', '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(10, 'delete_menus', 'menus', '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(11, 'browse_roles', 'roles', '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(12, 'read_roles', 'roles', '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(13, 'edit_roles', 'roles', '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(14, 'add_roles', 'roles', '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(15, 'delete_roles', 'roles', '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(16, 'browse_users', 'users', '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(17, 'read_users', 'users', '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(18, 'edit_users', 'users', '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(19, 'add_users', 'users', '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(20, 'delete_users', 'users', '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(21, 'browse_settings', 'settings', '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(22, 'read_settings', 'settings', '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(23, 'edit_settings', 'settings', '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(24, 'add_settings', 'settings', '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(25, 'delete_settings', 'settings', '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(26, 'browse_categories', 'categories', '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(27, 'read_categories', 'categories', '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(28, 'edit_categories', 'categories', '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(29, 'add_categories', 'categories', '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(30, 'delete_categories', 'categories', '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(31, 'browse_posts', 'posts', '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(32, 'read_posts', 'posts', '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(33, 'edit_posts', 'posts', '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(34, 'add_posts', 'posts', '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(35, 'delete_posts', 'posts', '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(36, 'browse_pages', 'pages', '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(37, 'read_pages', 'pages', '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(38, 'edit_pages', 'pages', '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(39, 'add_pages', 'pages', '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(40, 'delete_pages', 'pages', '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(41, 'browse_hooks', NULL, '2019-06-29 13:18:38', '2019-06-29 13:18:38'),
(42, 'browse_habers', 'habers', '2019-07-01 20:24:37', '2019-07-01 20:24:37'),
(43, 'read_habers', 'habers', '2019-07-01 20:24:37', '2019-07-01 20:24:37'),
(44, 'edit_habers', 'habers', '2019-07-01 20:24:37', '2019-07-01 20:24:37'),
(45, 'add_habers', 'habers', '2019-07-01 20:24:37', '2019-07-01 20:24:37'),
(46, 'delete_habers', 'habers', '2019-07-01 20:24:37', '2019-07-01 20:24:37'),
(47, 'browse_duyurus', 'duyurus', '2019-07-02 05:23:05', '2019-07-02 05:23:05'),
(48, 'read_duyurus', 'duyurus', '2019-07-02 05:23:05', '2019-07-02 05:23:05'),
(49, 'edit_duyurus', 'duyurus', '2019-07-02 05:23:05', '2019-07-02 05:23:05'),
(50, 'add_duyurus', 'duyurus', '2019-07-02 05:23:05', '2019-07-02 05:23:05'),
(51, 'delete_duyurus', 'duyurus', '2019-07-02 05:23:05', '2019-07-02 05:23:05'),
(52, 'browse_yayins', 'yayins', '2019-07-02 06:01:57', '2019-07-02 06:01:57'),
(53, 'read_yayins', 'yayins', '2019-07-02 06:01:57', '2019-07-02 06:01:57'),
(54, 'edit_yayins', 'yayins', '2019-07-02 06:01:57', '2019-07-02 06:01:57'),
(55, 'add_yayins', 'yayins', '2019-07-02 06:01:57', '2019-07-02 06:01:57'),
(56, 'delete_yayins', 'yayins', '2019-07-02 06:01:57', '2019-07-02 06:01:57'),
(57, 'browse_galeries', 'galeries', '2019-07-02 08:24:09', '2019-07-02 08:24:09'),
(58, 'read_galeries', 'galeries', '2019-07-02 08:24:09', '2019-07-02 08:24:09'),
(59, 'edit_galeries', 'galeries', '2019-07-02 08:24:09', '2019-07-02 08:24:09'),
(60, 'add_galeries', 'galeries', '2019-07-02 08:24:09', '2019-07-02 08:24:09'),
(61, 'delete_galeries', 'galeries', '2019-07-02 08:24:09', '2019-07-02 08:24:09'),
(62, 'browse_videos', 'videos', '2019-07-02 12:21:58', '2019-07-02 12:21:58'),
(63, 'read_videos', 'videos', '2019-07-02 12:21:58', '2019-07-02 12:21:58'),
(64, 'edit_videos', 'videos', '2019-07-02 12:21:58', '2019-07-02 12:21:58'),
(65, 'add_videos', 'videos', '2019-07-02 12:21:58', '2019-07-02 12:21:58'),
(66, 'delete_videos', 'videos', '2019-07-02 12:21:58', '2019-07-02 12:21:58'),
(67, 'browse_sayfas', 'sayfas', '2019-07-03 10:55:40', '2019-07-03 10:55:40'),
(68, 'read_sayfas', 'sayfas', '2019-07-03 10:55:40', '2019-07-03 10:55:40'),
(69, 'edit_sayfas', 'sayfas', '2019-07-03 10:55:40', '2019-07-03 10:55:40'),
(70, 'add_sayfas', 'sayfas', '2019-07-03 10:55:40', '2019-07-03 10:55:40'),
(71, 'delete_sayfas', 'sayfas', '2019-07-03 10:55:40', '2019-07-03 10:55:40'),
(72, 'browse_forum_categories', 'forum_categories', '2019-07-03 12:13:11', '2019-07-03 12:13:11'),
(73, 'read_forum_categories', 'forum_categories', '2019-07-03 12:13:11', '2019-07-03 12:13:11'),
(74, 'edit_forum_categories', 'forum_categories', '2019-07-03 12:13:11', '2019-07-03 12:13:11'),
(75, 'add_forum_categories', 'forum_categories', '2019-07-03 12:13:11', '2019-07-03 12:13:11'),
(76, 'delete_forum_categories', 'forum_categories', '2019-07-03 12:13:11', '2019-07-03 12:13:11'),
(77, 'browse_messages', 'messages', '2019-07-03 12:19:43', '2019-07-03 12:19:43'),
(78, 'read_messages', 'messages', '2019-07-03 12:19:43', '2019-07-03 12:19:43'),
(79, 'edit_messages', 'messages', '2019-07-03 12:19:43', '2019-07-03 12:19:43'),
(80, 'add_messages', 'messages', '2019-07-03 12:19:43', '2019-07-03 12:19:43'),
(81, 'delete_messages', 'messages', '2019-07-03 12:19:43', '2019-07-03 12:19:43'),
(82, 'browse_anlasmas', 'anlasmas', '2019-07-03 20:00:23', '2019-07-03 20:00:23'),
(83, 'read_anlasmas', 'anlasmas', '2019-07-03 20:00:23', '2019-07-03 20:00:23'),
(84, 'edit_anlasmas', 'anlasmas', '2019-07-03 20:00:23', '2019-07-03 20:00:23'),
(85, 'add_anlasmas', 'anlasmas', '2019-07-03 20:00:23', '2019-07-03 20:00:23'),
(86, 'delete_anlasmas', 'anlasmas', '2019-07-03 20:00:23', '2019-07-03 20:00:23'),
(87, 'browse_kuruls', 'kuruls', '2019-07-04 13:18:36', '2019-07-04 13:18:36'),
(88, 'read_kuruls', 'kuruls', '2019-07-04 13:18:36', '2019-07-04 13:18:36'),
(89, 'edit_kuruls', 'kuruls', '2019-07-04 13:18:36', '2019-07-04 13:18:36'),
(90, 'add_kuruls', 'kuruls', '2019-07-04 13:18:36', '2019-07-04 13:18:36'),
(91, 'delete_kuruls', 'kuruls', '2019-07-04 13:18:36', '2019-07-04 13:18:36'),
(92, 'browse_basins', 'basins', '2019-07-04 14:46:37', '2019-07-04 14:46:37'),
(93, 'read_basins', 'basins', '2019-07-04 14:46:37', '2019-07-04 14:46:37'),
(94, 'edit_basins', 'basins', '2019-07-04 14:46:37', '2019-07-04 14:46:37'),
(95, 'add_basins', 'basins', '2019-07-04 14:46:37', '2019-07-04 14:46:37'),
(96, 'delete_basins', 'basins', '2019-07-04 14:46:37', '2019-07-04 14:46:37'),
(97, 'browse_baskan_mesajs', 'baskan_mesajs', '2019-07-06 23:10:28', '2019-07-06 23:10:28'),
(98, 'read_baskan_mesajs', 'baskan_mesajs', '2019-07-06 23:10:28', '2019-07-06 23:10:28'),
(99, 'edit_baskan_mesajs', 'baskan_mesajs', '2019-07-06 23:10:28', '2019-07-06 23:10:28'),
(100, 'add_baskan_mesajs', 'baskan_mesajs', '2019-07-06 23:10:28', '2019-07-06 23:10:28'),
(101, 'delete_baskan_mesajs', 'baskan_mesajs', '2019-07-06 23:10:28', '2019-07-06 23:10:28');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1),
(53, 1),
(54, 1),
(55, 1),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(62, 1),
(63, 1),
(64, 1),
(65, 1),
(66, 1),
(67, 1),
(68, 1),
(69, 1),
(70, 1),
(71, 1),
(72, 1),
(73, 1),
(74, 1),
(75, 1),
(76, 1),
(77, 1),
(78, 1),
(79, 1),
(80, 1),
(81, 1),
(82, 1),
(83, 1),
(84, 1),
(85, 1),
(86, 1),
(87, 1),
(88, 1),
(89, 1),
(90, 1),
(91, 1),
(92, 1),
(93, 1),
(94, 1),
(95, 1),
(96, 1),
(97, 1),
(98, 1),
(99, 1),
(100, 1),
(101, 1);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `author_id`, `category_id`, `title`, `seo_title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `featured`, `created_at`, `updated_at`) VALUES
(1, 0, NULL, 'Lorem Ipsum Post', NULL, 'This is the excerpt for the Lorem Ipsum Post', '<p>This is the body of the lorem ipsum post</p>', 'posts/post1.jpg', 'lorem-ipsum-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(2, 0, NULL, 'My Sample Post', NULL, 'This is the excerpt for the sample Post', '<p>This is the body for the sample post, which includes the body.</p>\n                <h2>We can use all kinds of format!</h2>\n                <p>And include a bunch of other stuff.</p>', 'posts/post2.jpg', 'my-sample-post', 'Meta Description for sample post', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(3, 0, NULL, 'Latest Post', NULL, 'This is the excerpt for the latest post', '<p>This is the body for the latest post</p>', 'posts/post3.jpg', 'latest-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(4, 0, NULL, 'Yarr Post', NULL, 'Reef sails nipperkin bring a spring upon her cable coffer jury mast spike marooned Pieces of Eight poop deck pillage. Clipper driver coxswain galleon hempen halter come about pressgang gangplank boatswain swing the lead. Nipperkin yard skysail swab lanyard Blimey bilge water ho quarter Buccaneer.', '<p>Swab deadlights Buccaneer fire ship square-rigged dance the hempen jig weigh anchor cackle fruit grog furl. Crack Jennys tea cup chase guns pressgang hearties spirits hogshead Gold Road six pounders fathom measured fer yer chains. Main sheet provost come about trysail barkadeer crimp scuttle mizzenmast brig plunder.</p>\n<p>Mizzen league keelhaul galleon tender cog chase Barbary Coast doubloon crack Jennys tea cup. Blow the man down lugsail fire ship pinnace cackle fruit line warp Admiral of the Black strike colors doubloon. Tackle Jack Ketch come about crimp rum draft scuppers run a shot across the bow haul wind maroon.</p>\n<p>Interloper heave down list driver pressgang holystone scuppers tackle scallywag bilged on her anchor. Jack Tar interloper draught grapple mizzenmast hulk knave cable transom hogshead. Gaff pillage to go on account grog aft chase guns piracy yardarm knave clap of thunder.</p>', 'posts/post4.jpg', 'yarr-post', 'this be a meta descript', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2019-06-29 13:18:37', '2019-06-29 13:18:37');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(2, 'user', 'Normal User', '2019-06-29 13:18:37', '2019-06-29 13:18:37');

-- --------------------------------------------------------

--
-- Table structure for table `sayfas`
--

CREATE TABLE `sayfas` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` longtext COLLATE utf8mb4_unicode_ci,
  `hit` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sayfas`
--

INSERT INTO `sayfas` (`id`, `title`, `body`, `hit`, `created_at`, `updated_at`, `slug`) VALUES
(1, 'Deneme sayfa 2', '<p>Deneme i&ccedil;erik</p>', 10, '2019-07-03 10:56:00', '2019-07-03 11:06:17', 'deneme-sayfa-2'),
(2, 'Mevzuat', '<p>Mevzuat</p>', 18, '2019-07-04 14:19:50', '2019-07-06 23:41:22', 'mevzuat'),
(3, 'Hukuk', '<p>Hukuk</p>', 24, '2019-07-04 14:20:04', '2019-07-06 23:25:41', 'hukuk');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Site Title', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', '', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', NULL, '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', '', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Öz Finans-İş', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Yönetim Paneli', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', '', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', NULL, '', 'text', 1, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `translations`
--

INSERT INTO `translations` (`id`, `table_name`, `column_name`, `foreign_key`, `locale`, `value`, `created_at`, `updated_at`) VALUES
(1, 'data_types', 'display_name_singular', 5, 'pt', 'Post', '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(2, 'data_types', 'display_name_singular', 6, 'pt', 'Página', '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(3, 'data_types', 'display_name_singular', 1, 'pt', 'Utilizador', '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(4, 'data_types', 'display_name_singular', 4, 'pt', 'Categoria', '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(5, 'data_types', 'display_name_singular', 2, 'pt', 'Menu', '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(6, 'data_types', 'display_name_singular', 3, 'pt', 'Função', '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(7, 'data_types', 'display_name_plural', 5, 'pt', 'Posts', '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(8, 'data_types', 'display_name_plural', 6, 'pt', 'Páginas', '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(9, 'data_types', 'display_name_plural', 1, 'pt', 'Utilizadores', '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(10, 'data_types', 'display_name_plural', 4, 'pt', 'Categorias', '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(11, 'data_types', 'display_name_plural', 2, 'pt', 'Menus', '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(12, 'data_types', 'display_name_plural', 3, 'pt', 'Funções', '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(13, 'categories', 'slug', 1, 'pt', 'categoria-1', '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(14, 'categories', 'name', 1, 'pt', 'Categoria 1', '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(15, 'categories', 'slug', 2, 'pt', 'categoria-2', '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(16, 'categories', 'name', 2, 'pt', 'Categoria 2', '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(17, 'pages', 'title', 1, 'pt', 'Olá Mundo', '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(18, 'pages', 'slug', 1, 'pt', 'ola-mundo', '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(19, 'pages', 'body', 1, 'pt', '<p>Olá Mundo. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(20, 'menu_items', 'title', 1, 'pt', 'Painel de Controle', '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(21, 'menu_items', 'title', 2, 'pt', 'Media', '2019-06-29 13:18:37', '2019-06-29 13:18:37'),
(22, 'menu_items', 'title', 12, 'pt', 'Publicações', '2019-06-29 13:18:38', '2019-06-29 13:18:38'),
(23, 'menu_items', 'title', 3, 'pt', 'Utilizadores', '2019-06-29 13:18:38', '2019-06-29 13:18:38'),
(24, 'menu_items', 'title', 11, 'pt', 'Categorias', '2019-06-29 13:18:38', '2019-06-29 13:18:38'),
(25, 'menu_items', 'title', 13, 'pt', 'Páginas', '2019-06-29 13:18:38', '2019-06-29 13:18:38'),
(26, 'menu_items', 'title', 4, 'pt', 'Funções', '2019-06-29 13:18:38', '2019-06-29 13:18:38'),
(27, 'menu_items', 'title', 5, 'pt', 'Ferramentas', '2019-06-29 13:18:38', '2019-06-29 13:18:38'),
(28, 'menu_items', 'title', 6, 'pt', 'Menus', '2019-06-29 13:18:38', '2019-06-29 13:18:38'),
(29, 'menu_items', 'title', 7, 'pt', 'Base de dados', '2019-06-29 13:18:38', '2019-06-29 13:18:38'),
(30, 'menu_items', 'title', 10, 'pt', 'Configurações', '2019-06-29 13:18:38', '2019-06-29 13:18:38');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`) VALUES
(1, 1, 'Admin', 'admin@admin.com', 'users/default.png', NULL, '$2y$10$HtIq8p26XmLyCwCF0RA7zODXZBdG5Gxk5wc0wxlF41UrdREhBvf9q', 'ZOT5iOeS6W5fEq6X34ayrdFq6jBKSIvrtQEaPTGMIGtmFDNBK4oBWAL6i996', '{\"locale\":\"tr\"}', '2019-06-29 13:18:37', '2019-07-02 21:47:48');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE `videos` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `videos` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `lang` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'tr'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `videos`
--

INSERT INTO `videos` (`id`, `title`, `slug`, `image`, `videos`, `created_at`, `updated_at`, `lang`) VALUES
(1, 'Deneme video', 'deneme-video', 'videos\\July2019\\xMoQhFo6tfw3vYuam3Dm.jpg', '[{\"download_link\":\"videos\\\\July2019\\\\CDGsk40GgDn9gcF1LFLB.mp4\",\"original_name\":\"59840563_430521017781191_3056200536291278848_n.mp4\"}]', '2019-07-02 12:24:09', '2019-07-02 12:24:09', 'tr');

-- --------------------------------------------------------

--
-- Table structure for table `yayins`
--

CREATE TABLE `yayins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `yayins`
--

INSERT INTO `yayins` (`id`, `name`, `image`, `file`, `url`, `created_at`, `updated_at`) VALUES
(1, 'AbdulSamet ŞAHİN', 'yayins\\July2019\\nz87IP4UbATEWZSJRi54.jpg', '[{\"download_link\":\"yayins\\\\July2019\\\\Q52AcRemV4azmvZEVCPD.mp4\",\"original_name\":\"59840563_430521017781191_3056200536291278848_n.mp4\"}]', NULL, '2019-07-02 06:05:11', '2019-07-02 06:05:11');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `anlasmas`
--
ALTER TABLE `anlasmas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `basins`
--
ALTER TABLE `basins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `baskan_mesajs`
--
ALTER TABLE `baskan_mesajs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`),
  ADD KEY `categories_parent_id_foreign` (`parent_id`);

--
-- Indexes for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indexes for table `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indexes for table `duyurus`
--
ALTER TABLE `duyurus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `forum_categories`
--
ALTER TABLE `forum_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `galeries`
--
ALTER TABLE `galeries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `habers`
--
ALTER TABLE `habers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kuruls`
--
ALTER TABLE `kuruls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_slug_unique` (`slug`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `posts_slug_unique` (`slug`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `sayfas`
--
ALTER TABLE `sayfas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `yayins`
--
ALTER TABLE `yayins`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `anlasmas`
--
ALTER TABLE `anlasmas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `basins`
--
ALTER TABLE `basins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `baskan_mesajs`
--
ALTER TABLE `baskan_mesajs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=156;

--
-- AUTO_INCREMENT for table `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `duyurus`
--
ALTER TABLE `duyurus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `forum_categories`
--
ALTER TABLE `forum_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `galeries`
--
ALTER TABLE `galeries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `habers`
--
ALTER TABLE `habers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `kuruls`
--
ALTER TABLE `kuruls`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sayfas`
--
ALTER TABLE `sayfas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `yayins`
--
ALTER TABLE `yayins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
